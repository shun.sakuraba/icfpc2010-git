

import sys


for l in sys.stdin:
    prev = ' '
    cnt = 0
    for c in l:
        if c == '\n':
            break
        if c == prev:
            cnt += 1
        else:
            if cnt == 1 or prev == ' ':
                sys.stdout.write(prev)
            else:
                sys.stdout.write("%c^%d" % (prev, cnt))
            prev = c
            cnt = 1
    if cnt == 1 or prev == ' ':
        sys.stdout.write(prev)
    else:
        sys.stdout.write("%c^%d" % (prev, cnt))
    sys.stdout.write('\n')


            
