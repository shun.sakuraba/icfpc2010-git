#!/usr/bin/python

import sys
import cStringIO as StringIO

def ParsePrefix(is_length, line):
    if line[0] == '0':
        return (0, line[1:])
    if line[0] == '1':
        if is_length:
            return (1, line[1:])
        else:
            return (1 + int(line[1]), line[2:])
    assert line[0:2] == '22', line
    n, line = ParsePrefix(is_length, line[2:])
    if not is_length:
        n += 2
    value = (3 ** n - 1) / 2
    if n != 0:
        value += int(line[0:n], 3)
    if is_length:
        value += 2
    return (value, line[n:])

def ParseLength(line):
    return ParsePrefix(True, line)

def ParseValue(line):
    return ParsePrefix(False, line)

def ParseList(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (value, line) = ParseValue(line)
        result.append(value)
    return (result, line)
    

def ParseMatrix(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (value, line) = ParseList(line)
        result.append(value)
    return (result, line)

def ParseFuel(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (value, line) = ParseMatrix(line)
        result.append(value)
    assert line == '', line
    return result

def PrintFuel(fuel):
    print '%d %d' % (len(fuel), len(fuel[0]))
    print ''
    for mat in fuel:
        for line in mat:
            print ' '.join(str(v) for v in line)
        print ''

def main():
    for line in sys.stdin:
        PrintFuel(ParseFuel(line.strip()))

if __name__ == '__main__':
    main()
