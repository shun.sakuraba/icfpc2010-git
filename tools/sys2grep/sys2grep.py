#!/usr/bin/python

import sys
import urllib

url = 'http://purepure.coders.jp/'
params = urllib.urlencode(dict(action='grep', pat=sys.argv[1]))
sys.stdout.write(urllib.urlopen(url + '?' + params).read())
