#!/usr/bin/python

import cgi
import commands
import re
import os
import sys

def PostFuel(carid, fuel):
    if not os.path.isdir("./log/%s" % carid):
        os.makedirs("./log/%s" % carid)
    if os.path.isfile("./log/%s/%s.txt" % (carid, fuel)):
        f = open("./log/%s/%s.txt" % (carid, fuel), 'r')
        print f.read()
        return
    cmdline = "echo \"[1,1,0,2,1,2,1,0,1,1,2,1,0,1,2,2,1] %s\" | ./bin/generate > ./log/%s/%s.cir && echo \"\n \" >> ./log/%s/%s.cir && HOME=`pwd`/log ./bin/runcir %s ./log/%s/%s.cir" % (fuel, carid, fuel, carid, fuel, carid, carid, fuel)
    (status, output) = commands.getstatusoutput(cmdline)
    if status:
        os.remove("./log/%s/%s.txt" % (carid, fuel))
        raise Exception(output)
    f = open("./log/%s/%s.txt" % (carid, fuel), "w")
    f.write(output)
    f.close()
    print output

def main():
    print "Content-Type: text/plain"
    print
    try:
        form = cgi.FieldStorage()
        carid = form["carid"].value
        fuel = form["fuel"].value
        fuel = re.compile(r'\s', re.DOTALL).sub("", fuel)
        assert re.compile(r'^[0-9]+$', re.DOTALL).match(carid)
        assert re.compile(r'^[0-2]+$', re.DOTALL).match(fuel)
        PostFuel(carid, fuel)
    except Exception, e:
        print "Server Error:"
        print sys.exc_info()[0]
        print sys.exc_info()[1]
        print sys.exc_info()[2]

if __name__ == '__main__':
    main()
