#!/usr/bin/python

import sys,os,re,cgi
import encfuel
import StringIO

TH = 30

def PrintEntry(e):
    titlemsg = e['msg'].replace("\n", "&nbsp;")
    print "<a style='text-decoration: none' href=\"%s\" title=\"%s\">" % ("log/" + e['fname'], titlemsg)
    if e['good']:
        print "<font color=green>"
    else:
        print "<font color=red>"
    sfuel = e['fuel']
    if len(sfuel) >= TH:
        sfuel = sfuel[0:TH] + "(ry"
    print sfuel
    print "</font>"
    print "<font color=gray>"
    print e['lastmsg']
    print "</font>"
    print "</span>"

def GetLogCar(carid):
    fuels = dict()
    accepted = False
    sysfile = carid + "/SYSTEM"
    sys = ""
    if os.path.isfile(sysfile):
        f = open(sysfile)
        sys = f.read()
        f.close()
    usersfile = carid + "/USERS"
    users = "0"
    if os.path.isfile(usersfile):
        f = open(usersfile)
        users = f.read()
        f.close()
    for fuel in sorted(os.listdir(carid)):
        fname = carid + "/" + fuel
        if fuel.endswith(".txt"):
            f = open(fname, 'r')
            res = f.read()
            f.close()
            fuel = fuel[:-4]
            msg = re.compile(r"^.*this is a legal prefix\n", re.DOTALL).sub("", res)
            msg = re.compile(r"\s*$", re.DOTALL).sub("", msg)
            lastmsg = re.compile(r"^.*\n", re.DOTALL).sub("", msg)
            good = re.compile(r"Good!", re.DOTALL).search(res)
            mtime = os.path.getmtime(fname)
            fuels[fuel] = dict(fuel=fuel, msg=msg, lastmsg=lastmsg, good=good, mtime=mtime, fname=fname)
            if good:
                accepted = True
    return (fuels, accepted, sys, users)

def PrintLogCar(carid):
    (fuels, accepted, sys, users) = GetLogCar(carid)
    n = len(fuels)
    fuel_keys = sorted(fuels.keys())
    cols = 2
    rows = (n+cols-1)/cols
    print "<div style='margin: 1em'>"
    print "<div style='margin: 1em 0em'>"
    print "<span style='font-size: large; font-weight: bold'>"
    print "<a name='%s'>" % carid
    if accepted:
        print "<font color=blue>%s</font>" % carid
    else:
        print "<font color=red>%s</font>" % carid
    print "</a>"
    print "</span>"
    print "<span><font color=gray>(%d)</font> <a href='log/%s/SPEC'><font color=gray>spec</font></a></span> <a href='?action=submit&amp;carid=%s'><font color=gray>submit</font></a></span>" % (len(fuels), carid, carid)
    print "<div style='padding-left: 1em'><pre>%s</pre></div>" % sys
    print "</div>"
    print "<table style='font-size: small' width='100%%'>"
    for row in range(rows):
        print "<tr>"
        for col in range(cols):
            i = row + col*rows
            if i >= n:
                continue
            e = fuels[fuel_keys[i]]
            print "<td width='%d%%' style='margin-right: 3px'>" % (100/cols)
            PrintEntry(e)
            print "</td>"
        print "</tr>"
    print "</table></div>"


def PrintLogList():
    fuel_all = dict()
    for carid in os.listdir("."):
        if not os.path.isdir(carid):
            continue
        fuel_all[carid] = GetLogCar(carid)
    def popularity_cmp(a, b):
        if fuel_all[a][3] != fuel_all[b][3]:
            return -cmp(int(fuel_all[a][3]), int(fuel_all[b][3]))
        return cmp(int(a), int(b))
    fuel_keys = list(fuel_all.keys())
    fuel_keys.sort(popularity_cmp)
    print "<div>Unsolved: "
    for carid in fuel_keys:
        (fuels, accepted, sys, users) = fuel_all[carid]
        if not accepted:
            print "<a href='#%s'>%s</a>" % (carid, carid)
    print "</div>"
    for carid in fuel_keys:
        (fuels, accepted, sys, users) = fuel_all[carid]
        print "<div style='margin: 0.5em'>"
        print "<span style='font-size: normal; font-weight: bold'>"
        print "<a name='%s' href='?action=car&amp;carid=%s' style='text-decoration: none'>" % (carid, carid)
        if accepted:
            print "<font color=blue>%s</font>" % carid
        else:
            print "<font color=red>%s</font>" % carid
        print "</span></a>"
        print "<span><font color=black>%s users</font> <font color=gray>(%d)</font> <a href='log/%s/SPEC'><font color=gray>spec</font></a></span> <a href='?action=submit&amp;carid=%s'><font color=gray>submit</font></a></span>" % (users, len(fuels), carid, carid)
        if not accepted:
            print "<div style='padding-left: 1em'><pre>%s</pre></div>" % sys
        print "</div>"

def PrintSubmitForm(carid):
    print """
<form action="runfuel.cgi" method="post">
CarID: %s<br>
<input type="hidden" name="carid" value="%s">
RAW Fuel:<br>
<textarea name="fuel" rows="10" cols="80"></textarea>
<br>
<input type="submit">
</form>
""" % (carid, carid)
    print """<pre>
# Input Format:
#
# The first line contains two integers M and N, which indicates the number
# of tanks and air components respectively.  Then M blocks follow; each block
# contains N lines consisting of N integers and represents a fuel operator.
#
# The input must be given from the standard input.  Each line can contain
# a comment starting with '#'.</pre>
"""
    print """
<form action="printlog2.cgi" method="post">
<input type="hidden" name="carid" value="%s">
<input type="hidden" name="action" value="convert_submit">
Text Fuel:<br>
<textarea name="textfuel" rows="10" cols="80"></textarea>
<br>
<input type="submit">
</form>
""" % carid

def ConvertAndRedirect(carid, fuel):
    old_stdin = sys.stdin
    sys.stdin = StringIO.StringIO(fuel)
    res = encfuel.encode(encfuel.scan())
    sys.stdin = old_stdin
    print "<meta http-equiv='Refresh' content='0;runfuel.cgi?carid=%s&amp;fuel=%s'>" % (carid, res)


def main():

    os.chdir("log")

    form = cgi.FieldStorage()

    print "Content-Type: text/html"
    print

    action = ""
    if 'action' in form:
        action = form['action'].value
    carid = None
    if 'carid' in form:
        carid = form["carid"].value
        assert re.compile(r'^[0-9]+$', re.DOTALL).match(carid)

    if action == "submit":
        PrintSubmitForm(carid)
    elif action == 'convert_submit':
        ConvertAndRedirect(carid, form['textfuel'].value)
    elif action == 'car':
        PrintLogCar(carid)
    else:
        PrintLogList()

    print "</body></html>"


if __name__ == '__main__':
    try:
        main()
    except:
        print sys.exc_info()
