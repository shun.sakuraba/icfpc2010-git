#!/usr/bin/python

import sys,os,re

print "Content-Type: text/html"
print


print "<html><body>"

os.chdir("log")
for carid in os.listdir("."):
    if not os.path.isdir(carid):
        continue
    fuels = []
    print "<h2>%s</h2><table border=1>" % carid
    for fuel in sorted(os.listdir(carid)):
        if fuel.endswith(".txt"):
            f = open(carid + "/" + fuel)
            res = f.read()
            f.close()
            fuel = fuel[:-4]
            res = re.compile(r"^.*this is a legal prefix\n", re.DOTALL).sub("", res)
            res = re.compile(r"\s*$", re.DOTALL).sub("", res)
            print "<tr><td><pre>%s</pre></td><td><pre>%s</pre></td></tr>" % (fuel, res)
    print "</table>"

print "</body></html>"
