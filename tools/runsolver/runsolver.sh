#!/bin/bash

PWD=`pwd`
ICFPCDIR=${PWD%/icfppc2010*}/icfppc2010

# TODO VALIDATE carid

carid=$1
cmd=$2
fuel=`wget -O - http://purepure.coders.jp/log/${carid}/SPEC | \
      ${cmd} | \
      python ${ICFPCDIR}/tools/encfuel/encfuel.py`
wget -O - -q "http://purepure.coders.jp/submit.cgi?carid=$carid&fuel=$fuel"
