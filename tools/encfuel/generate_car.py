#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Generate car spec with given fuel spec.

Input Format:

The first line contains two integers M and N, which indicates the number
of tanks and air components respectively.  Then M blocks follow; each block
contains N lines consisting of N integers and represents a fuel operator.

The input must be given from the standard input.  Each line can contain
a comment starting with '#'.

Output format:
car: car spec.
fuel: fuel spec in trit, not in circuit.

"""

import encfuel
import validator

import itertools
import math
from optparse import OptionParser
import random
import subprocess
import sys
import time

def multMInPlace(m1, m2, result):
    size = len(m1)
    for i in xrange(size):
        for j in xrange(size):
            s = 0
            for k in xrange(size):
                s += m1[k][i] * m2[j][k]
            result[j][i] = s
    #print 'm1 = ', m1,
    #print 'm2 = ', m2
    #print 'result', result


def multM(m1, m2):
    result = [[0 for x in xrange(size)] for y in xrange(size)]
    multMInPlace(m1, m2, result)
    return result


def multMatricesFoldReverse(ms):
    """ [Matrix] -> Matrix.  fold mult I (reverse ms)."""
    size = len(ms[0])
    base   = [[0 for x in xrange(size)] for y in xrange(size)]
    result = [[0 for x in xrange(size)] for y in xrange(size)]
    for i in xrange(size):
        base[i][i] = 1
    for m in ms:
        multMInPlace(m, base, result)
        base, result = result, base
    return base


def subM(m1, m2):
    size = len(m1)
    result = [[0 for x in xrange(size)] for y in xrange(size)]
    for i in xrange(size):
        for j in xrange(size):
            result[i][j] = m1[i][j] - m2[i][j]
    return result


def minValue(m):
    result = m[0][0]
    for vec in m:
        result = min([result] + vec)
    return result


def isValidChamber(fuel, sequence1, sequence2, is_main):
    """Check if it's valid chamber or not.
    
    Returns:
      (is_valid, [s1, is_main, s2], score)
    """
    m01 = multMatricesFoldReverse([fuel[i] for i in sequence1])
    m02 = multMatricesFoldReverse([fuel[i] for i in sequence2])
    #print 'm01 = ', m01
    #print 'm02 = ', m02
    # Try this in two ways to speed up.
    for s1, s2, m1, m2 in [(sequence1, sequence2, m01, m02),
                           (sequence2, sequence1, m02, m01)]:
        diff = subM(m1, m2)
        s = minValue(diff)
        if s < 0:
            continue
        if is_main and diff[0][0] == 0:
            continue
        if diff[0][0] > 0:
            become_aux = 0
        else:
            become_aux = 1
        return  (True, [s1, become_aux, s2], s)
    return (False, [sequence1, 0, sequence2], -1)


def generateChamber(fuel, items, is_main):
    """Generate random chamber with given fuel set and number of factors.

    Args:
      fuel: list of fuels.
      items: number of factors in expression = number of sections.
      is_main: If true, this should be main.

    Returns: (chamber, score)
      chamber: [upper pipe, is_main, lower pipe]
      score: smallest coefficient.
    """
    q = 0
    while True:
        q += 1
        # print 'trial %d' % q
        seq01 = [random.randrange(len(fuel)) for i in xrange(items)]
        seq02 = [random.randrange(len(fuel)) for i in xrange(items)]
        #print 'seq01: ', seq01
        #print 'seq02: ', seq02
        (judge, result, score) = isValidChamber(fuel, seq01, seq02, is_main)
        #print 'judge = ', judge, score
        if judge:
            return (result, score)


def flatten1(iterables):
    for it in iterables:
        for element in it:
            yield element


def permutations(iterable, r=None):
    # permutations('ABCD', 2) --> AB AC AD BA BC BD CA CB CD DA DB DC
    # permutations(range(3)) --> 012 021 102 120 201 210
    pool = tuple(iterable)
    n = len(pool)
    r = n if r is None else r
    if r > n:
        return
    indices = range(n)
    cycles = range(n, n-r, -1)
    yield tuple(pool[i] for i in indices[:r])
    while n:
        for i in reversed(range(r)):
            cycles[i] -= 1
            if cycles[i] == 0:
                indices[i:] = indices[i+1:] + indices[i:i+1]
                cycles[i] = n - i
            else:
                j = cycles[i]
                indices[i], indices[-j] = indices[-j], indices[i]
                yield tuple(pool[i] for i in indices[:r])
                break
        else:
            return


def encodeCar(car):
    # use iterable.chain.
    return (encfuel.encode_len(len(car)) +
            ''.join(encfuel.encode(x) for x in flatten1(car)))


def tupleChamber(chamber):
    return (tuple(chamber[0]), chamber[1], tuple(chamber[2]))


def generateCar(fuel, trials, chambers, expressions):
    chamber_list = []
    chamber_pool = set()
    for i in xrange(trials):
        sys.stderr.write('generating chamber %d\r' % i)
        while True:
            chamber, score = generateChamber(fuel, expressions, False)
            c_tuple = tupleChamber(chamber)
            if c_tuple in chamber_pool:
                continue
            chamber_list.append((chamber, score))
            chamber_pool.add(c_tuple)
            break
    assert len(chamber_list) >= chambers
    chamber_list.sort(key = lambda ch_score: ch_score[1])  # sort by score.
    sub_chambers = [chamber for chamber, score in chamber_list[:chambers - 1]]
    # add main chamber.
    main_chamber = None
    while True:
        chamber, score = generateChamber(fuel, expressions, True)
        c_tuple = tupleChamber(chamber)
        if c_tuple in chamber_pool:
            continue
        main_chamber = chamber
        break
    return [main_chamber] + sub_chambers


def reorderTank(car, fuel):
    """Reorder chambers and fuel tanks to get minimum car."""
    fuel_size = len(fuel)
    record_encode = '9'
    record_car = ''
    record_order = []
    for fuel_order in permutations(range(fuel_size)):
        new_car = []
        # Make new car with changing fuel order.
        for chamber in car:
            p1 = [fuel_order[s] for s in chamber[0]]
            p2 = [fuel_order[s] for s in chamber[2]]
            new_car.append([p1, chamber[1], p2])
        # sort chambers.
        new_car.sort(key=encfuel.encode)
        new_car_code = encfuel.encode(new_car)
        if new_car_code < record_encode:  # new car is smaller.
            record_encode = new_car_code
            record_car = new_car
            record_order = fuel_order
    # Now we get new car with new encode.
    # Need to reorder fuels.
    new_fuel = [None] * fuel_size
    for i in xrange(fuel_size):
        new_fuel[record_order[i]] = fuel[i]
    return record_car, new_fuel


class CarGenerator(object):
    def __init__(self):
        pass

    def generate(self):
        pass  # Please override this.


class CarFromFuelGenerator(CarGenerator):
    def __init__(self):
        self.chambers = 99
        self.trials = 2000
        self.expressions = 30
        self.fuel = []

    def generate(self):
        car = generateCar(self.fuel, self.trials, self.chambers,
                          self.expressions)
        return reorderTank(car, self.fuel)


def runSolver(car, path, command_line):
    """Run command line solver."""
    enccar = encfuel.encode(car)
    # sys.stderr.write('in = %s' % enccar)
    solver_process = subprocess.Popen(command_line,
                                      cwd=path,
                                      shell=True,
                                      stdin=subprocess.PIPE,
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE
                                      )
    (out_data, err_data) = solver_process.communicate(enccar)
    sys.stderr.write('solver_result:' + str(err_data) + '\n')
    if solver_process.returncode == 0 and out_data != '':
        return out_data
    return None


class CarGeneratorWithSolvers(CarGenerator):
    def __init__(self):
        self.solver_base = ''
        self.solvers = ['sa', 'sa_logpenalty']
        self.generator = None

    def generate(self):
        ntry = 0
        while True:
            ntry += 1
            sys.stderr.write('solver trial #%d\n' % ntry)
            car, fuel = self.generator.generate()
            passed = True
            for s_dir in self.solvers:
                path = '%s/%s' % (self.solver_base, s_dir)
                # args = [path + '/run.sh', '--long']
                args = '%s/run.sh --long' % path
                sys.stderr.write('trying solver %s\n' % args[0])
                result = runSolver(car, path, args)
                if result is not None:
                    sys.stderr.write('solver failed by %s: %s\n' %
                                     (s_dir, result))
                    passed = False
                    break
            if passed:
                return (car, fuel)


def generateOneFuelComponent(dim, mean):
    result = [[int(random.expovariate(1.0/mean)) for i in xrange(dim)]
              for j in xrange(dim)]
    if result[0][0] == 0:
        result[0][0] += 1
    return result


def generateRandomFuel(count=0, dim=0):
    if count == 0:
        count = random.randrange(2, 7)
    if dim == 0:
        dim = random.randrange(2, 4)
    return [generateOneFuelComponent(dim, 12) for i in xrange(count)]


def main(argv):
    parser = OptionParser()
    parser.add_option('-f', '--use_fuel', action='store_true', dest='use_fuel')
    parser.add_option('-t', '--trials', type='int', dest='trials',
                      default=2000)
    parser.add_option('-c', '--chambers', type='int', dest='chambers',
                      default=99)
    parser.add_option('-e', '--expressions', type='int', dest='expressions',
                      default=20)
    parser.add_option('-p', '--parser', action='store_true', dest='use_parser')
    parser.add_option('--circuit', action='store_true', dest='use_circuit')
    parser.add_option('--solver_base', dest='solver_base',
                      default='../../solver')
    parser.add_option('--solver', action='store_true', dest='use_solver')

    (options, args) = parser.parse_args()
    fuel = None
    if options.use_fuel:
        fuel = encfuel.scan()
    else:
        fuel = generateRandomFuel()
    print 'original_fuel:', fuel
    print 'options:', options
    sys.stdout.flush()

    generator = CarFromFuelGenerator()
    generator.chambers = options.chambers
    generator.trials = options.trials
    generator.expressions = options.expressions
    generator.fuel = fuel

    if options.use_solver:
        sol = CarGeneratorWithSolvers()
        sol.solver_base = options.solver_base
        sol.generator = generator
        generator = sol

    car, fuel = generator.generate()

    # print 'defcar;', car
    print 'deffuel', fuel
    encoded_car = encodeCar(car)
    print 'car:', encoded_car
    encoded_fuel = encfuel.encode(fuel)
    print 'fuel:', encoded_fuel

    print 'validate:',
    sys.stdout.flush()
    print validator.validate(encoded_car, fuel)

    sys.stdout.flush()
    if options.use_parser:
        print 'printablecar:'
        sys.stdout.flush()
        subprocess.call(['../../circuit/car_parser.py', encoded_car])
    if options.use_circuit:
        print 'circuit:'
        sys.stdout.flush()
        magic='11021210112101221'
        subprocess.call('echo %s %s | runhaskell ../../circuit/generator/Gen.hs'
                        % (magic, encoded_fuel), shell=True)
    #print 'circuit_fuel: '


if __name__ == '__main__':
    main(sys.argv)
