#! /usr/bin/python

import car_parser
import encfuel
import sys


FUEL_LEN_LIMIT = 1000
FUEL_NUM_LIMIT = 6


def permutations(L):
    if len(L) == 1:
        yield [L[0]]
    elif len(L) >= 2:
        (a, b) = (L[0:1], L[1:])
        for p in permutations(b):
            for i in xrange(len(p) + 1):
                yield p[:i] + a + p[i:]


class RegularMatrix:
    def __init__(self, cols):
        self.n = len(cols)
        if not all(len(col) == self.n for col in cols):
            raise ValueError
        self.cols = cols

    def __add__(x, y):
        cols = []
        for i in xrange(x.n):
            cols.append([0] * x.n)
            for j in xrange(x.n):
                cols[i][j] = x.cols[i][j] + y.cols[i][j]
        return RegularMatrix(cols)

    def __sub__(x, y):
        cols = []
        for i in xrange(x.n):
            cols.append([0] * x.n)
            for j in xrange(x.n):
                cols[i][j] = x.cols[i][j] - y.cols[i][j]
        return RegularMatrix(cols)

    def __mul__(x, y):
        cols = []
        for i in xrange(x.n):
            cols.append([0] * x.n)
            for j in xrange(x.n):
                for k in xrange(x.n):
                    cols[i][j] += x.cols[k][j] * y.cols[i][k]
        return RegularMatrix(cols)

    def __repr__(self):
        return self.cols.__repr__()

    @staticmethod
    def identity(n):
        cols = []
        for i in xrange(n):
            cols.append([j == i for j in xrange(n)])
        return RegularMatrix(cols)


class Tank:
    def __init__(self, matrix, index):
        if not 0 <= index < 6: raise ValueError
        self.matrix = matrix
        self.index = index


class Chamber:
    def __init__(self, upper, isaux, lower):
        self.upper = upper
        self.isaux = isaux
        self.lower = lower

    def validate(self):
        n = self.upper[0].matrix.n
        um = RegularMatrix.identity(n)
        for tank in self.upper: um = tank.matrix * um
        lm = RegularMatrix.identity(n)
        for tank in self.lower: lm = tank.matrix * lm
        dm = um - lm
        if (not self.isaux) and dm.cols[0][0] <= 0:
            return False
        if any(any(x < 0 for x in col) for col in dm.cols):
            return False
        return True


class Car:
    def __init__(self):
        self.chambers = []

    def add_chamber(self, chamber):
        self.chambers.append(chamber)

    def encode(self):
        min_code = '~'
        min_perm = None
        for perm in permutations(range(0, 6)):
            encoded_chambers = []
            for chamber in self.chambers:
                upper = encfuel.encode([perm[x.index] for x in chamber.upper])
                isaux = encfuel.encode(chamber.isaux)
                lower = encfuel.encode([perm[x.index] for x in chamber.lower])
                encoded_chambers.append(upper + isaux + lower)
            encoded_chambers.sort()
            code = ''.join(encoded_chambers)
            if min_code > code:
                min_code, min_perm = code, perm  # update
        len_code = encfuel.encode_len(len(self.chambers))
        return (len_code + min_code, min_perm)


def validate(car_code, fuel_data):
    if len(fuel_data) > FUEL_NUM_LIMIT:
        return False, 'too many tanks'
    car_spec = car_parser.ParseCar(car_code)
    tanks = []
    for i in xrange(len(fuel_data)):
        tanks.append(Tank(RegularMatrix(fuel_data[i]), i))
    car = Car()
    for upper, is_main, lower in car_spec:
        upper = [tanks[i] for i in upper]
        lower = [tanks[i] for i in lower]
        chamber = Chamber(upper, not is_main, lower)
        if not chamber.validate():
            return False, 'fuels does not run the car'
        car.add_chamber(chamber)
    if car_code != car.encode()[0]:
        return False, 'invalid car code'
    if len(encfuel.encode(fuel_data)) > FUEL_LEN_LIMIT:
        return False, 'too long fuel'
    return True, 'good'


def main():
    car_code = sys.stdin.readline().strip()
    fuel_data = encfuel.scan()
    result, message = validate(car_code, fuel_data)
    print message


if __name__ == '__main__':
    main()
