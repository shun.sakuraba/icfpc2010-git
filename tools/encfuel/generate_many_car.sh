#!/bin/bash
HEADER="$1"
for I in `seq 40`; do
    DATAFILE="CAR${HEADER}-${I}.txt"
    ./generate_car.py --circuit | tee $DATAFILE
    for D in sa sa_logpenalty; do
        cat $DATAFILE | awk '/^car:/{print $2}' | \
            ( cd ../../solver/$D; ./run.sh --long ) | tee "SOLVE${HEADER}-${I}-${D}.txt"
    done
    CIRCUIT2="CIRCUIT2-${HEADER}-${I}.txt"
    echo '11021210112101221' $(cat $DATAFILE | awk '/^fuel:/{print $2}') | runhaskell Gen.hs > $CIRCUIT2
done
