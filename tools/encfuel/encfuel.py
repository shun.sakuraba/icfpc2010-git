#! /usr/bin/python

# Input Format:
#
# The first line contains two integers M and N, which indicates the number
# of tanks and air components respectively.  Then M blocks follow; each block
# contains N lines consisting of N integers and represents a fuel operator.
#
# The input must be given from the standard input.  Each line can contain
# a comment starting with '#'.
#
# Warning:
#
# The matrcies used to need to be transposed, but should not now.

import re
import sys

def ternary(num, ndigits):
    s = ""
    for i in xrange(ndigits):
        s = str(num % 3) + s
        num /= 3
    return s

def encode_len(num):
    if num < 2:
        return ['0', '1'][num]
    offset = 2
    digits = -1
    while offset <= num:
        digits += 1
        offset += 3 ** digits
    return '22' + encode_len(digits) + ternary(num - offset, digits)

def encode_val(num):
    if num < 4:
        return ['0', '10', '11', '12'][num]
    offset = 0
    digits = -1
    while offset <= num:
        digits += 1
        offset += 3 ** digits
    return '22' + encode_val(digits - 2) + ternary(num - offset, digits)

def encode(data):
    if isinstance(data, int):
        return encode_val(data)
    if isinstance(data, long):
        return encode_val(data)
    if isinstance(data, list):
        return encode_len(len(data)) + ''.join(encode(x) for x in data)
    raise ValueError

def scan():
    lines = []
    for line in sys.stdin:
        lines.append(re.compile('#.*$').sub('', line))  # strip comments
    seq = [int(s) for s in ''.join(lines).split()]
    m = seq[0] ; del seq[0]
    n = seq[0] ; del seq[0]
    data = []
    for i in xrange(m):
        mat = []
        for j in xrange(n):
            mat.append([None] * n)
        for j in xrange(n):
            for k in xrange(n):
                mat[k][j] = seq[0] ; del seq[0]
        data.append(mat)
    assert seq == []
    return data

if __name__ == '__main__':
    print encode(scan())
