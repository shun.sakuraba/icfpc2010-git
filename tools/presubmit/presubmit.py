#!/usr/bin/python

import sys
import re
import os
import mechanize

if len(sys.argv) != 3:
    print "usage: %s carid cirfile" % sys.argv[0]
    sys.exit(1)

carid = sys.argv[1]
cirfile = sys.argv[2]

cir = open(cirfile, 'r').read().strip()
car = open("/home/icfp/public_html/log/%s/SPEC" % carid, 'r').read().strip()

cookie_file = "/home/icfp/.runcir.cookie"

m = mechanize.Browser()

submit_url = "http://nfa.imn.htwk-leipzig.de/icfpcont/"

result = m.open(submit_url).read()

if not re.search("hotspot", result):
    print "error"
    print result
    sys.exit(1)

m.select_form(nr=0)
m["G0"] = car
m["G1"] = cir
result = m.submit().read()

if not re.search("hotspot", result):
    print "error"
    print result
    sys.exit(1)

text = re.search(r'.*<pre\s*>(.*)</pre', result, re.DOTALL).group(1)
text = text.replace("&quot;", "\"")
text = text.replace("&gt;", ">")
text = text.replace("&lt;", "<")

print text
