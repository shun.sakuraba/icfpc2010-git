#include <iostream>
#include <algorithm>

using namespace std;

const int MAX = 12*11*10*9*8*7*6*5*4*3*2*1;

const int N = 2 * 6;
const int M = 17;

const int iseq[M] = { 0,1,2,0,2,1,0,1,2,1,0,2,0,1,2,0,2 };
const int oseq[M] = { 1,1,0,2,1,2,1,0,1,1,2,1,0,1,2,2,1 };

static int p[N];
static int x;

inline bool simulate()
{
	int q[M] = { 0 };

	for(int i = 0; i < M; i++) {
		q[x] = iseq[i];
		for(int j = 0; j < N; j += 2) {
			int ql = q[j+0];
			int qr = q[j+1];
			q[p[j+0]] = (ql - qr + 3) % 3;
			q[p[j+1]] = (ql * qr + 2) % 3;
		}
		if(q[x] != oseq[i])
			return false;
	}

	return true;
}

bool brute_force()
{
	for(int i = 0; i < N; i++)
		p[i] = i;

	int k = 0;
	do {
		for(/* int */ x = 0; x < N; x++) {
			if(simulate()) { return true; }
		}
		if((++k) % 100000 == 0) {
			cerr << k << " / " << MAX << "\r";
		}
	}
	while(next_permutation(p, p + N));

	return false;
}

int main()
{
	if(brute_force()) {
		for(int i = 0; i < N; i++)
			cout << p[i] << " ";
		cout << ": " << x << endl;
	}
	return 0;
}
