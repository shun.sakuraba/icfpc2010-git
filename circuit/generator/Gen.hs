import System.IO.Unsafe
import Control.Monad
import Data.Char
import Data.IORef
import Data.List
import Data.Maybe
import Text.Printf

rcnt = unsafePerformIO $ newIORef 0
redges = unsafePerformIO $ newIORef []

gate = do
  cnt <- readIORef rcnt
  writeIORef rcnt (cnt+1)
  return (((cnt, 0), (cnt, 1)), ((cnt, 0), (cnt, 1)))

conn from to = do
  modifyIORef redges ((from, to):)

seq1 g1 g2 = do
  (i1, o1) <- g1
  (i2, o2) <- g2
  
  conn o1 i2
  
  return (i1 :: (Int, Int), o2 :: (Int, Int))

identity = do
  (i1, o1) <- gate
  (i2, o2) <- gate
  (i3, o3) <- gate
  
  conn (snd o1) (fst i2)
  conn (fst o2) (fst i3)
  conn (snd o2) (snd i3)
  conn (snd o3) (snd i2)
  conn (fst o3) (snd i1)
  
  return (fst i1, fst o1)

gneg = do
  (i1, o1) <- gate
  (i2, o2) <- gate
  (i3, o3) <- gate
  
  conn (snd o1) (fst i2)
  conn (fst o2) (fst i3)
  conn (snd o2) (snd i3)
  conn (fst o3) (fst i1)
  conn (snd o3) (snd i2)
  
  return (snd i1, fst o1)

gif_1 = do
  (i1, o1) <- gate
  (i2, o2) <- gate
  
  conn (fst o1) (fst i2)
  conn (snd o1) (snd i2)
  
  return (i1, o2)

gif_2 = gif_1

gconst 0 = do
  (i1, o1) <- gate
  (i2, o2) <- gate
  (i3, o3) <- gif_2
  (i4, o4) <- gif_1
  
  conn (fst o1) (fst i2)
  conn (snd o1) (snd i2)
  conn (fst o2) (snd i4)
  conn (snd o2) (fst i3)
  conn (snd o3) (fst i4)
  conn (fst o4) (fst i1)
  conn (snd o4) (snd i1)
  
  return (snd i3, fst o3)

gconst 1 = seq1 (gconst 2) gneg

gconst 2 = do
  (i1, o1) <- gate
  (i2, o2) <- gconst 0
  
  conn (fst o1) i2
  conn o2 (fst i1)
  
  return (snd i1, snd o1)

gsub n = do
  (i1, o1) <- gconst n
  (i2, o2) <- gate
  
  conn (snd o2) i1
  conn o1 (snd i2)
  
  return (fst i2, fst o2)

gnone = do
  (i1, o1) <- gate
  
  conn (fst o1) (fst i1)
  conn (snd o1) (snd i1)
  
  return ()

-----

buildCircuit p = do
  (i, o) <- p
  cnt   <- readIORef rcnt
  edges <- readIORef redges
  printf "%s:\n" (ppos i)
  forM_ [0..cnt-1] $ \i -> do
    printf "%s%s0#%s%s%s\n"
      (findInput edges (i, 0))
      (findInput edges (i, 1))
      (findOutput edges (i, 0))
      (findOutput edges (i, 1))
      (if i==cnt-1 then ":" else ",")
  printf "%s\n" (ppos o)
  
  where
    findInput :: [((Int, Int), (Int, Int))] -> (Int, Int) -> String
    findInput es ix =
      fromMaybe "X" (find (\(f, t) -> ix==t) es >>= return . ppos . fst)
    findOutput :: [((Int, Int), (Int, Int))] -> (Int, Int) -> String
    findOutput es ix =
      fromMaybe "X" (find (\(f, t) -> ix==f) es >>= return . ppos . snd)
    
    ppos (ix, dir) = show ix ++ (if dir==0 then "L" else "R")

--

conn2 o i = do
  conn (fst o) (fst i)
  conn (snd o) (snd i)

conn2r o i = do
  conn (fst o) (snd i)
  conn (snd o) (fst i)

const2 0 0 = do
  (i1, o1) <- gate
  (i2, o2) <- gate
  (i3, o3) <- gate
  conn (snd o1) (snd i3)
  conn (fst o2) (fst i1)
  conn (snd o2) (fst i3)
  conn (fst o3) (snd i1)
  conn (snd o3) (snd i2)
  return (fst i2, fst o1)

const2 0 1 = do
  (i1, o1) <- gate
  (i2, o2) <- gate
  (i3, o3) <- gate
  (i4, o4) <- gate
  (i5, o5) <- gate
  (i6, o6) <- gif_2
  (i7, o7) <- gif_1
  conn2 o1 i2
  conn2r o2 i3
  conn (fst o3) (snd i5)
  conn (snd o3) (snd i7)
  conn (snd o4) (fst i6)
  conn (fst o5) (fst i4)
  conn (snd o5) (snd i6)
  conn (fst o6) (snd i4)
  conn (snd o6) (fst i7)
  conn2 o7 i1
  return (fst i5, fst o4)

const2 0 2 = do
  (i1, o1) <- gconst 0
  (i2, o2) <- gate
  (i3, o3) <- gate
  conn o1 (snd i2)
  conn (snd o2) (snd i3)
  conn (fst o3) (fst i2)
  conn (snd o3) i1
  return (fst i3, fst o2)

const2 1 0 = do
  (i1, o1) <- gate
  (i2, o2) <- gate
  (i3, o3) <- gate
  (i4, o4) <- gate
  (i5, o5) <- gate
  (i6, o6) <- gif_2
  (i7, o7) <- gif_1
  conn2 o1 i2
  conn2r o2 i3
  conn (fst o3) (snd i4)
  conn (snd o3) (snd i7)
  conn (snd o4) (fst i6)
  conn (fst o5) (fst i4)
  conn (snd o5) (snd i6)
  conn (fst o6) (snd i5)
  conn (snd o6) (fst i7)
  conn2 o7 i1
  return (fst i5, fst o4)

const2 1 1 = do
  (i1, o1) <- gsub 1
  (i2, o2) <- gsub 1
  conn o2 i1
  return (i2, o1)

const2 1 2 = do
  (i1, o1) <- gate
  (i2, o2) <- gate
  (i3, o3) <- gate
  (i4, o4) <- gate
  (i5, o5) <- gconst 0
  (i6, o6) <- gconst 0
  conn2 o1 i2
  conn (fst o2) (snd i3)
  conn (snd o2) (snd i4)
  conn (snd o3) i6
  conn (fst o4) (fst i3)
  conn (snd o4) i5
  conn o5 (fst i1)
  conn o6 (snd i1)
  return (fst i4, fst o3)

const2 2 0 = do
  (i1, o1) <- gate
  (i2, o2) <- gate
  (i3, o3) <- gconst 0
  (i4, o4) <- gate
  (i5, o5) <- gate
  (i6, o6) <- gate
  conn (fst o1) (snd i4)
  conn (snd o1) (snd i2)
  conn (snd o2) i3
  conn o3 (snd i5)
  conn (fst o4) (fst i2)
  conn (snd o4) (fst i5)
  conn2 o5 i6
  conn2 o6 i1
  return (fst i4, fst o2)

const2 2 1 = do
  (i1, o1) <- gate
  (i2, o2) <- gate
  (i3, o3) <- gate
  (i4, o4) <- gate
  (i5, o5) <- gconst 0
  (i6, o6) <- gconst 0
  conn2 o1 i2
  conn (fst o2) (snd i4)
  conn (snd o2) (snd i3)
  conn (snd o3) i6
  conn (fst o4) (fst i3)
  conn (snd o4) i5
  conn o5 (fst i1)
  conn o6 (snd i1)
  return (fst i4, fst o3)  

const2 2 2 = do
  (i1, o1) <- gsub 2
  (i2, o2) <- gsub 2
  conn o2 i1
  return (i2, o1)

constSequence :: [Int] -> IO ((Int, Int), (Int, Int))
constSequence nums = do
  let ds = [ ((b-c))`mod`3 | (c, b) <- zip nums (0:nums)]
  ds <- return [(a, b) | (ix, (a, b)) <- zip [0..] (zip ds (tail ds)), ix`mod`2==0]
      
  tansis <- forM ds $ \(d1, d2) -> do
    -- print (d1, d2)
    -- readIORef rcnt >>= print
    const2 d1 d2
  
  forM_ (zip tansis (tail tansis)) $ \((i1, o1), (i2, o2)) -> do
    conn o2 i1
  
  -- print tansis
  
  -- (isource, osource) <- gconst ( (- (last nums)) `mod` 3 )
  -- conn osource (fst $ last tansis)
  -- return (isource, snd $ head tansis)

  return (fst $ last tansis, snd $ head tansis)
    
main = do
  l <- getLine
  let ns = map (\c -> read [c]) $ filter isDigit l
  buildCircuit $ constSequence ns -- [1,1,0,2,1,2,1,0,1,1,2,1,0,1,2,2,1]
  -- buildCircuit $ gconst 1
  return ()
