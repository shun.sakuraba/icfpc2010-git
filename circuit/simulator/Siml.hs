import Text.Parsec
import Data.Char
import Debug.Trace

parseCircuit = do
  inum <- num
  idir <- oneOf "LR"
  char ':'
  eol
  
  elms <- many1 elm
  
  onum <- num
  odir <- oneOf "LR"
  eol

  return ((inum, idir), (onum, odir), elms)

elm = try $ do
  il <- conn
  ir <- conn
  string "0#"
  ol <- conn
  or <- conn
  oneOf ":,"
  eol
  return ((il, ir), (ol, or))

conn = (char 'X' >> return (-1, 'X')) <|> do
  n <- num
  d <- oneOf "LR"
  return (n, d)

num = do
  s <- many1 digit
  return $ (read s :: Int)

eol = char '\n'

--

exec (inp, out, circuit) tbl ins =
  let res = go initval ins in
  -- traceShow (zip res ins) $
  map fst res
  where
    go _ [] = []
    go cur (inp:inps) =
      let next = execStep inp cur circuit tbl in
      ((getout next out, cur): go next inps)
    
    getout vals (i, 'L') = fst $ vals!!i
    getout vals (i, 'R') = snd $ vals!!i
    
    n = length circuit
    initval = replicate n (0, 0)

execStep inp vals circuit tbl = ret
  where
    ret = [ calc (getinp i il) (getinp i ir)
          | i <- [0..n-1]
          , let ((il, ir), (ol, or)) = circuit!!i]
          
    getinp ix (-1, _) = inp
    getinp ix (i, 'L')
      | i<ix = fst $ ret!!i
      | otherwise = fst $ vals!!i
    getinp ix (i, 'R')
      | i<ix = snd $ ret!!i
      | otherwise = snd $ vals!!i

    calc ix jx = tbl!!ix!!jx
    
    n = length circuit

main = do
  con <- getContents
  let (t1:t2:t3:ins:rest) = lines con
  let tbl = map ((\[a,b,c,d,e,f]->[(a,b),(c,d),(e,f::Int)]) . map read . words . map sp) [t1, t2, t3]
  let inps = map (read :: String -> Int) $ words $ map sp ins
  -- print tbl
  -- print inps
  case parse parseCircuit "" (unlines rest) of
    Left err -> print err
    Right c -> do
      -- print c
      let result = exec c tbl inps
      print result
  return ()

sp c | isDigit c = c
     | otherwise = ' '
