#!/usr/bin/python
#
# This script generates a circuit which forms:
#
#   in    +---+
#    |    |   |
#  +--------+ |
#  | gate 0 | |
#  +--------+ |
#    |    |   |
#  +--------+ |
#  | gate 1 | |
#  +--------+ |
#    |    |   |
#  +--------+ |
#  | gate 2 | |
#  +--------+ |
#    |    |   |
#    :    :   :
#    |    |   |
#  +--------+ |
#  | gate n | |
#  +--------+ |
#    |    |   |
#   out   +---+
#

def main():
    n = 7
    print "0L:"
    print "X%dR0#1L1R," % (n)
    for i in xrange(n-1):
        print "%dL%dR0#%dL%dR," % (i, i, i+2, i+2)
    print "%dL%dR0#X0R:" % (n-1, n-1)
    print "%dL" % (n)

if __name__ == "__main__":
    main()
