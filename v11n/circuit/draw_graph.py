# The tool to generate dot file for circuit.
# How to use:
#   python draw_graph.py < example.txt > example.dot
#   dot -Tpng example.dot > example.png

import sys
import re

CONNECTOR = r'(\d+[LR]|X)'
PATTERN = CONNECTOR + CONNECTOR + r'0#' + CONNECTOR + CONNECTOR

def Parse(input):
    print 'digraph g {'
    print 'node [shape=record];'
    print 'begin [shape=record,label="begin"];'
    print 'end [shape=record,label="end"];'

    lines = [line for line in input]
    begin = lines[0].strip()[:-1]
    end = lines[-1].strip()
    gates = lines[1:-1]

    for index in xrange(len(gates)):
        print 'gate%d [shape=record,label="<L>|<C>%d|<R>"];' % (index, index)

    assert begin != "X"
    assert end != "X"
    print 'begin -> gate%s:%s;' % (begin[:-1], begin[-1])
    #    print 'end -> gate%d:%s;' %(int(end[:-1]), end[-1])

    ptn = re.compile(PATTERN)
    for (index, gate) in enumerate(gates):
        m = ptn.match(gate.strip()[:-1])
        # For now we ignore "inputs" here.
        if m.group(3) == 'X':
            print "gate%d:L -> end;" % index
        else:
            print "gate%d:L -> gate%s:%s;" % (
                index, m.group(3)[:-1], m.group(3)[-1])
        if m.group(4) == 'X':
            print "gate%d:R -> end;" % index
        else:
            print "gate%d:R -> gate%s:%s;" % (
                index, m.group(4)[:-1], m.group(4)[-1])
    print "}"

def main():
    Parse(sys.stdin)

if __name__ == "__main__":
    main();
