#!/bin/bash

solver=$1
caridfile=$2

solverdir="`pwd`/../solver/$solver"
runfuel="`pwd`/../tools/runfuel/runfuel"
encfuel="`pwd`/../tools/encfuel/encfuel.py"

if [ -z "$solver" -o ! -d $solverdir ]; then
    echo "solver not found: $solver"
    exit 1
fi

if [ ! -f "$caridfile" ]; then
    echo "caridfile not found: $caridfile"
    exit 1
fi

pushd $solverdir
if ! make; then
    echo "make failed"
    exit 1
fi
popd

cat $caridfile | while read carid; do
    echo -n "$carid: "
    cachedir="`pwd`/specs.$solver"
    mkdir -p "$cachedir"
    specfile="$cachedir/$carid.spec"
    outfile="$cachedir/$carid.out.$solver"
    resfile="$cachedir/$carid.result.$solver"
    wget -O "$specfile" -q "http://purepure.coders.jp/log/$carid/SPEC"
    pushd "$solverdir" > /dev/null 2>&1
    ./run.sh < "$specfile" > "$outfile" 2> /dev/null
    popd > /dev/null 2>&1
    if [ -s "$outfile" ]; then
        fuel=`$encfuel < $outfile`
        $runfuel $carid $fuel > $resfile 2> /dev/null
        if cat $resfile | grep -q 'cached'; then
            echo "CACHED"
        elif cat $resfile | grep -q 'Good'; then
            /bin/echo -e "\033[1;44mACCEPTED!\033[0m"
        else
            echo "FAILED [BUG]"
        fi
    else
        echo "FAILED"
    fi
done
