#!/bin/bash

if [ "$1" = "" ]; then
    echo "give me pattern"
    exit 1
fi

grep -- "$1" ~/src/icfp2010/backup/log/*/SYSTEM2 | sed 's/:.*//' | sed -e 's#.*log/##' -e 's#/.*##' | uniq
