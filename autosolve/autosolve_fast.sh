#!/bin/bash

runfuel=`pwd`/../tools/runfuel/runfuel
encfuel=`pwd`/../tools/encfuel/encfuel.py

hardmode=false
longmode=false
while [ "$1" != "" ]; do
    if [ "$1" = "--hard" ]; then
        hardmode=true
        longmode=true
        shift
        continue
    fi
    if [ "$1" = "--long" ]; then
        longmode=true
        shift
        continue
    fi
    echo "unknown option: $1"
    exit 1
done

longopt=""
fetchopt=""
if $longmode; then
    longopt="--long"
fi
if $hardmode; then
    fetchopt="&choice=hard"
fi

pushd `pwd`/../solver > /dev/null 2>&1
solvers="12_solver  aaa_solver  ab_solver  cbf_solver  pow_solver special3 special5"

echo "Setting up solvers..."
for solver in $solvers; do
    echo "$solver:"
    pushd $solver > /dev/null 2>&1
    if make; then
        :
    else
        echo "$solver: Compile Error!"
        exit 1
    fi
    popd > /dev/null 2>&1
done
popd > /dev/null 2>&1
pushd `pwd`/trivial > /dev/null 2>&1
trivials=`ls *`
popd > /dev/null 2>&1

echo

while :; do
    carids=`wget -q -O - "http://purepure.coders.jp/?action=unsolved$fetchopt" | sort -rn`
    processed=false
    for carid in $carids; do
        specfile="`pwd`/specs/$carid.spec"
        if [ -f "$specfile" ]; then continue; fi
        touch $specfile
        processed=true
        echo "New unsolved problem: $carid"
        wget -q -O - "http://purepure.coders.jp/log/$carid/SPEC" > $specfile
        passed=false
        if eval $passed; then break; fi
        for solver in $solvers; do
            echo -n "  Trying: $solver ... "
            outfile="`pwd`/specs/$carid.out.$solver"
            resfile="`pwd`/specs/$carid.result.$solver"
            if [ -e "$resfile" ]; then
                echo "cached."
                continue
            fi
            pushd `pwd`/../solver/$solver > /dev/null 2>&1
            ./run.sh $longopt < $specfile > $outfile 2> /dev/null
            popd > /dev/null 2>&1
            if [ -s $outfile ]; then
                echo -n "solved! "
                fuel=`$encfuel < $outfile`
                $runfuel $carid $fuel > $resfile 2> /dev/null &
                /bin/echo -e "\033[1;44msubmitted!\033[0m"
            else
                echo "failed."
            fi
        done
        break
    done
    if [ $processed = false ]; then
        sleep 60
    fi
done

