#!/bin/bash

runfuel=../../tools/runfuel/runfuel

for carid in `wget -q -O - "http://purepure.coders.jp/?action=solved"`; do
    echo "Checking: $carid"
    for trivial in trivial/*; do
        echo "  trying $trivial "
        trivial=`basename $trivial`
        toutfile=$outfile.$trivial
        $runfuel $carid $trivial > $toutfile
        if grep -q 'cached' $toutfile; then
            echo "cached."
            continue
        fi
        if grep -q 'Good' $toutfile; then
            cat $toutfile
            passed=true
            break
        fi
        echo "failed."
    done
done


