#!/usr/bin/python

import cgi
import commands
import re
import os
import sys
import traceback
import hashlib

def PostFuel(carid, fuel, force, v):
    hash = hashlib.md5(fuel).hexdigest()
    cardir = "./log/%s" % carid
    if not os.path.isdir(cardir):
        os.makedirs(cardir)
    txtfile = "%s/%s.txt" % (cardir, hash)
    if not force and os.path.isfile(txtfile):
        f = open(txtfile, 'r')
        print "(cached)"
        print f.read()
        return
    fuelfile = "%s/%s.fuel" % (cardir, hash)
    f = open(fuelfile, 'w')
    f.write(fuel)
    f.close()
    cirfile = "%s/%s.cir" % (cardir, hash)
    cmdline = "touch %s; echo \"[1,1,0,2,1,2,1,0,1,1,2,1,0,1,2,2,1] %s\" | /home/icfp/bin/generate > %s && echo \"\n %s\" >> %s && HOME=/home/icfp /home/icfp/bin/runcir.py %s %s" % (txtfile, fuel, cirfile, ' '*v, cirfile, carid, cirfile)
    (status, output) = commands.getstatusoutput(cmdline)
    if status:
        os.remove(txtfile)
        raise Exception(output)
    f = open(txtfile, "w")
    f.write(output)
    f.close()
    os.utime(cardir, None)
    print output

def main():
    sys.stderr = sys.stdout
    print "Content-Type: text/plain"
    print
    try:
        form = cgi.FieldStorage()
        carid = form["carid"].value
        fuel = form["fuel"].value
        force = (form["force"].value == "YES") if 'force' in form else False
        v = int(form["v"].value) if 'v' in form else 0
        fuel = re.compile(r'\s', re.DOTALL).sub("", fuel)
        assert re.compile(r'^[0-9]+$', re.DOTALL).match(carid)
        assert re.compile(r'^[0-2]+$', re.DOTALL).match(fuel)
        PostFuel(carid, fuel, force, v)
    except:
        print "Server Error:"
        traceback.print_exc()


if __name__ == '__main__':
    main()
