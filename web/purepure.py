#!/usr/bin/python

import sys, os, re
import hashlib
import mechanize
import car_parser
import traceback
import cPickle, fcntl
import fuelparser
import cStringIO


class FileUtil(object):

    @classmethod
    def ReadFile(self, path):
        try:
            f = open(path)
            return f.read()
        except:
            return ""
        finally:
            try:
                f.close()
            except:
                pass

    @classmethod
    def WriteFile(self, path, text):
        try:
            f = open(path, 'w')
            f.write(text)
        finally:
            try:
                f.close()
            except:
                pass


class PurePureLog(object):

    def __init__(self):
        self.ourcars = FileUtil.ReadFile("../OURCARS").split()

    def HashFuel(self, fuel):
        fuel = fuel.strip()
        return hashlib.md5(fuel).hexdigest()

    def GetCarIDs(self):
        carids = list()
        for file in os.listdir("."):
            if os.path.isdir(file) and os.path.isfile(file + "/SPEC"):
                carids.append(file)
        return carids

    def GetLog(self, carid):
        cachefile = carid + ".cache"
        f = open(cachefile, 'a+b')
        fcntl.flock(f.fileno(), fcntl.LOCK_EX)
        f.seek(0)
        try:
            (cache_mtime, result) = cPickle.load(f)
        except:
            cache_mtime = None
        mtime = os.path.getmtime(carid)
        assert mtime is not None
        if cache_mtime != mtime:
            #print "cache unmatch %s: %s vs %s" % (carid, cache_mtime, mtime)
            result = self._GetLogNoCache(carid)
            f.seek(0, 0)
            f.truncate()
            cPickle.dump((mtime, result), f, protocol=-1)
        f.close()
        return result

    def _GetLogNoCache(self, carid):
        fuels = dict()
        accepted = False
        sys = FileUtil.ReadFile(carid + "/SYSTEM2") or ""
        users = FileUtil.ReadFile(carid + "/USERS").strip() or "0"
        ours = (carid in self.ourcars)
        for txtname in sorted(os.listdir(carid)):
            if not txtname.endswith(".txt"):
                continue
            fname = carid + "/" + txtname
            hash = txtname[:-4]
            fuel = FileUtil.ReadFile(carid + "/" + hash + ".fuel").strip()
            res = FileUtil.ReadFile(fname)
            msg = re.compile(r"^.*this is a legal prefix\n", re.DOTALL).sub("", res)
            msg = re.compile(r"\s*$", re.DOTALL).sub("", msg)
            lastmsg = re.compile(r"^.*\n", re.DOTALL).sub("", msg)
            good = bool(re.compile(r"Good!", re.DOTALL).search(res))
            mtime = os.path.getmtime(fname)
            fuels[fuel] = dict(fuel=fuel, carid=carid, hash=hash, msg=msg, lastmsg=lastmsg, good=good, mtime=mtime)
            if good:
                accepted = True
        return (fuels, accepted, sys, users, ours)

    def GrepCarSystem2(self, pat):
        carids = []
        for carid in os.listdir("."):
            sys2file = carid + "/SYSTEM2"
            if not os.path.isfile(sys2file):
                continue
            sys2 = FileUtil.ReadFile(sys2file)
            if re.search(pat, sys2) and not self.GetLog(carid)[1]:
                carids.append(int(carid))
        carids.sort()
        return map(str, carids)


class PurePureClient(object):

    COOKIE_FILE = "/home/icfp/.runcir.cookie"

    USERNAME = "pure pure code ++"
    PASSWORD = "649621105066043239504812479660330034666775488569025932788624"

    TOP_URL = "http://icfpcontest.org/icfp10/"
    CARS_URL = "http://icfpcontest.org/icfp10/score/instanceTeamCount"
    FUEL_URL = "http://icfpcontest.org/icfp10/instance/%s/solve/form"

    LOG_DIR = "/home/icfp/public_html/log"

    def __init__(self):
        self.cookie_jar = mechanize.LWPCookieJar()
        try:
            self.cookie_jar.load(self.COOKIE_FILE,
                                 ignore_discard=True,
                                 ignore_expires=True)
        except:
            pass
        self.br = mechanize.Browser()
        self.br.set_cookiejar(self.cookie_jar)

    def SaveCookie(self):
        self.cookie_jar.save(self.COOKIE_FILE,
                             ignore_discard=True,
                             ignore_expires=True)

    def Login(self):
        print "Logging in..."
        result = self.br.open(self.TOP_URL).read()
        self.br.follow_link(text_regex=r"Login")
        self.br.select_form(nr=0)
        self.br["j_username"] = self.USERNAME
        self.br["j_password"] = self.PASSWORD
        self.br.submit()

    def LoginAndOpen(self, url):
        result = self.br.open(url).read()
        if not re.search("Logout", result):
            self.Login()
            result = self.br.open(url).read()
        return result

    def UpdateTeamStatus(self):
        result = self.LoginAndOpen(self.TOP_URL)
        score = re.search(r'<div id="_score_id" class="box">(\d+(\.\d*)?)</div>', result).group(1)
        others_solved = re.search(r'<div id="_solution_id" class="box">(\d+)</div>', result).group(1)
        my_cars = re.search(r'<div id="_instance_id" class="box">(\d+)</div>', result).group(1)
        FileUtil.WriteFile("/home/icfp/public_html/status/SCORE", score)
        FileUtil.WriteFile("/home/icfp/public_html/status/SOLVED", others_solved)
        FileUtil.WriteFile("/home/icfp/public_html/status/MYCARS", my_cars)

    def UpdateCarList(self):
        self.UpdateCarSpecMissing()
        self.UpdateCarSpecIncremental()
        self.UpdateCarSystem()
        self.UpdateFuelMatrix()

    def _CarIDCmp(self, a, b):
        return cmp(int(a), int(b))

    def UpdateCarSpecMissing(self):
        all_carids = list()
        for file in os.listdir(self.LOG_DIR):
            if os.path.isdir(self.LOG_DIR + "/" + file):
                all_carids.append(file)
        all_carids.sort(self._CarIDCmp)
        nospec_carids = []
        for carid in all_carids:
            if not os.path.isfile("%s/%s/SPEC" % (self.LOG_DIR, carid)):
                nospec_carids.append(carid)
        nospec_carids.sort(self._CarIDCmp)
        specdone_carids = set()
        while nospec_carids:
            head = nospec_carids[0]
            if head in specdone_carids:
                del nospec_carids[0]
                continue
            specdone_carids.update(self.UpdateCarSpecAfter(str(int(head)-1)))

    def UpdateCarSpecIncremental(self):
        all_carids = list()
        for file in os.listdir(self.LOG_DIR):
            if os.path.isdir(self.LOG_DIR + "/" + file):
                all_carids.append(file)
        all_carids.sort(self._CarIDCmp)
        last_carid = all_carids[-1]
        while True:
            done = self.UpdateCarSpecAfter(last_carid)
            if not done:
                break
            last_carid = done[-1]

    def UpdateCarSpecAfter(self, head):
        result = self.br.open("http://nfa.imn.htwk-leipzig.de/recent_cars/?G0=%s&S1=submit" % head).read()
        if not re.search(r'hotspot', result):
            raise Exception("failed to fetch cars page")
        done = []
        for carid, spec in re.findall(r' >\((\d+),&quot;[^&]*&quot;,&quot;([012]*)&quot;\)</pre', result):
            cardir = "%s/%s" % (self.LOG_DIR, carid)
            if not os.path.isdir(cardir):
                os.makedirs(cardir)
            specfile = "%s/%s/SPEC" % (self.LOG_DIR, carid)
            if os.path.isfile(specfile):
                continue
            FileUtil.WriteFile(specfile, spec)
            print "SPEC:", carid
            done.append(carid)
        return done

    def UpdateCarSystem(self):
        all_carids = list()
        for file in os.listdir(self.LOG_DIR):
            if os.path.isfile(self.LOG_DIR + "/" + file + "/SPEC"):
                all_carids.append(file)
        for carid in all_carids:
            cardir = "%s/%s" % (self.LOG_DIR, carid)
            specfile = cardir + "/SPEC"
            sysfile = cardir + "/SYSTEM"
            sys2file = cardir + "/SYSTEM2"
            if os.path.isfile(sysfile) and os.path.isfile(sys2file):
                continue
            spec = FileUtil.ReadFile(specfile).strip()
            try:
                parsed = car_parser.ParseCar(spec)
            except:
                print "%s: SYSTEM GENERATION FAILED" % carid
                traceback.print_exc()
                continue
            sys = ""
            sys2 = ""
            for eq in car_parser.ToEquationList(parsed):
                sys += eq + "\n"
                eq = eq.replace("M[0]", "A")
                eq = eq.replace("M[1]", "B")
                eq = eq.replace("M[2]", "C")
                eq = eq.replace("M[3]", "D")
                eq = eq.replace("M[4]", "E")
                eq = eq.replace("M[5]", "F")
                sys2 += eq + "\n"
            FileUtil.WriteFile(sysfile, sys)
            FileUtil.WriteFile(sys2file, sys2)
            print "%s: SYSTEM generated" % carid

    def UpdateFuelMatrix(self):
        for carid in os.listdir(self.LOG_DIR):
            cardir = self.LOG_DIR + "/" + carid
            if not os.path.isdir(cardir):
                continue
            for fuelfile in os.listdir(cardir):
                fuelfile = cardir + "/" + fuelfile
                if not fuelfile.endswith(".fuel"):
                    continue
                matfile = fuelfile[:-5] + ".mat"
                if os.path.exists(matfile):
                    continue
                fuel = FileUtil.ReadFile(fuelfile)
                old_stdout = sys.stdout
                sys.stdout = cStringIO.StringIO()
                try:
                    fuelparser.PrintFuel(fuelparser.ParseFuel(fuel))
                    mat = sys.stdout.getvalue()
                    FileUtil.WriteFile(matfile, mat)
                    sys.stdout = old_stdout
                    print "%s: MATRIX generated" % carid
                except:
                    FileUtil.WriteFile(matfile, "illegal")
                    sys.stdout = old_stdout
                    print "%s: MATRIX illegal" % carid

    def UpdateCarListLegacy(self):
#        result = self.LoginAndOpen(self.CARS_URL)
#        for (carid, users) in re.findall(r'<td style="width: 20%;">(\d+)</td><td>(\d+)</td>', result):
#            cardir = "/home/icfp/public_html/log/%s" % carid
#            if not os.path.isdir(cardir):
#                os.makedirs(cardir)
#            FileUtil.WriteFile(cardir + "/USERS", users)
        carids = list()
        for file in os.listdir("/home/icfp/public_html/log"):
            if os.path.isdir("/home/icfp/public_html/log/" + file):
                carids.append(file)
#        for carid in carids:
#            cardir = "/home/icfp/public_html/log/%s" % carid
#            specfile = cardir + "/SPEC"
#            if os.path.isfile(specfile):
#                continue
#            result = self.LoginAndOpen(self.FUEL_URL % carid)
#            spec = re.search(r"Car:</label>([012]*)", result).group(1)
#            FileUtil.WriteFile(specfile, spec)
#            print "%s: SPEC" % carid
        for carid in carids:
            cardir = "/home/icfp/public_html/log/%s" % carid
            sysfile = cardir + "/SYSTEM"
            if os.path.isfile(sysfile):
                continue
            specfile = cardir + "/SPEC"
            if not os.path.isfile(specfile):
                continue
            spec = FileUtil.ReadFile(specfile).strip()
            try:
                parsed = car_parser.ParseCar(spec)
            except:
                print "%s: FAILED" % carid
                traceback.print_exc()
                continue
            f = open(sysfile, 'w')
            for eq in car_parser.ToEquationList(parsed):
                print >>f, eq
            f.close()
            print "%s: SYSTEM generated" % carid
        for carid in carids:
            cardir = "/home/icfp/public_html/log/%s" % carid
            sysfile = cardir + "/SYSTEM2"
            if os.path.isfile(sysfile):
                continue
            specfile = cardir + "/SPEC"
            if not os.path.isfile(specfile):
                continue
            spec = FileUtil.ReadFile(specfile).strip()
            try:
                parsed = car_parser.ParseCar(spec)
            except:
                print "%s: FAILED" % carid
                traceback.print_exc()
                continue
            f = open(sysfile, 'w')
            for eq in car_parser.ToEquationList(parsed):
                eq = eq.replace("M[0]", "A")
                eq = eq.replace("M[1]", "B")
                eq = eq.replace("M[2]", "C")
                eq = eq.replace("M[3]", "D")
                eq = eq.replace("M[4]", "E")
                eq = eq.replace("M[5]", "F")
                print >>f, eq
            f.close()
            print "%s: SYSTEM2 generated" % carid
