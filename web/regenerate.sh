#!/bin/bash

hg push yuno
ssh icfp 'cd hg; hg up'
ssh icfp 'cd hg/circuit/generator && rm *.o *.hi && ghc -o a.out --make -O2 Gen.hs && cp -v a.out /home/icfp/bin/generate'

wget -q -O - "http://purepure.coders.jp/testing/index.cgi?action=regenerate_list" | while read carid fuel; do
    echo "Regenerating $carid/$fuel"
    ../tools/runfuel/runfuel "$carid" "$fuel&force=YES"
done
