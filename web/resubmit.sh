#!/bin/bash

logdir=/home/nya/src/icfp2010/web/log
#logdir=/home/icfp/public_html/log

find $logdir -name '*.fuel' | while read fuelfile; do
    txtfile=${fuelfile%%.fuel}.txt
    if [ -e "$txtfile" -a -s "$txtfile" ]; then continue; fi
    carid=`echo "$fuelfile" | sed -e 's#.*log/##' -e 's#/.*##'`
    fuel=`cat "$fuelfile"`
    forceopt=""
    if [ -e "$txtfile" ]; then
        forceopt="&force=YES&v=1"
    fi
    echo -n "=== $carid: "
    if [ -n "$forceopt" ]; then
        echo "zero cache"
    else
        echo "no cache"
    fi
    if [ "$1" = "--do" ]; then
        ../tools/runfuel/runfuel "$carid" "$fuel$forceopt" &
    fi
done
if [ "$1" != "--do" ]; then
    echo "give me --do option to actually run"
fi
