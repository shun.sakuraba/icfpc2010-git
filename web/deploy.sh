#!/bin/sh

scp *.cgi *.py icfp:public_html/testing/

if [ "$1" = "-p" ]; then
    echo "*** PUSHING TO PRODUCTION ***"
    scp *.cgi *.py icfp:public_html/
fi

