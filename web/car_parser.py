#!/usr/bin/python
#

import sys
import cStringIO as StringIO

def ParsePrefix(is_length, line):
    if line[0] == '0':
        return (0, line[1:])
    if line[0] == '1':
        if is_length:
            return (1, line[1:])
        else:
            return (1 + int(line[1]), line[2:])
    assert line[0:2] == '22', line
    n, line = ParsePrefix(is_length, line[2:])
    if not is_length:
        n += 2
    value = (3 ** n - 1) / 2
    if n != 0:
        value += int(line[0:n], 3)
    if is_length:
        value += 2
    return (value, line[n:])

def ParseLength(line):
    return ParsePrefix(True, line)

def ParseValue(line):
    return ParsePrefix(False, line)

def ParsePipe(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (value, line) = ParseValue(line)
        result.append(value)
    return (result, line)
    
def ParseChamber(line):
    (upper, line) = ParsePipe(line)
    (flg, line) = ParseValue(line)
    (lower, line) = ParsePipe(line)
    return ((upper, flg == 0, lower), line)

def ParseCar(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (chamber, line) = ParseChamber(line)
        result.append(chamber)

    # All characters should be used.
    assert line == '', line;
    return result

def ToEquation((upper, flg, lower)):
    output = StringIO.StringIO()
    try:
        if upper:
            lst = ['M[%d]' % v for v in upper]
            lst.reverse()
            output.write(''.join(lst))
        else:
            output.write('I')
        output.write('-')
        if lower:
            lst = ['M[%d]' % v for v in lower]
            lst.reverse()
            output.write(''.join(lst))
        else:
            output.write('I')

        if flg:
            output.write(' > 0')
        else:
            output.write(' >= 0')

        return output.getvalue()
    finally:
        output.close()
def ToEquationList(car):
    return [ToEquation(chamber) for chamber in car]

def main(args):
    if len(args) > 1:
        input = args[1:]
    else:
        input = sys.stdin
    for arg in input:
        arg = arg.strip()
        for eq in ToEquationList(ParseCar(arg)):
            print eq
        print ''

if __name__ == "__main__":
    main(sys.argv)
