#!/usr/bin/python

import purepure

def main():
    client = purepure.PurePureClient()
    client.UpdateTeamStatus()
    client.UpdateCarList()
    client.SaveCookie()

if __name__ == '__main__':
    main()

