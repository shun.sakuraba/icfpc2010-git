#!/usr/bin/python

import sys, os, re, cgi
import traceback
import purepure
import encfuel
import cStringIO
import random


class PurePureWeb(object):

    MAX_NAME_LEN = 30
    COLS = 2

    def __init__(self):
        self.log = purepure.PurePureLog()

    def GetCarID(self):
        carid = self.form['carid'].value
        assert re.compile(r'^[0-9]+$', re.DOTALL).match(carid)
        return carid

    def PrintEntry(self, e):
        print "<a style='text-decoration: none' href=\"%s\" title=\"%s\">" % ("log/" + e['carid'] + "/" + e['hash'] + ".fuel", e['fuel'])
        if e['good']:
            print "<font color=green>"
        else:
            print "<font color=red>"
        fuellen = len(e['fuel'])
        sfuel = e['fuel']
        if len(sfuel) >= self.MAX_NAME_LEN:
            sfuel = sfuel[0:self.MAX_NAME_LEN] + "(ry"
        if not sfuel.strip():
            sfuel = "(no output)"
        print sfuel
        print "</font>"
        print "</a>"
        print "<a href='%s' style='text-decoration: none'><font color=black>[%d]</font></a>" % ("log/" + e['carid'] + "/" + e['hash'] + '.mat', fuellen)
        titlemsg = e['msg'].replace("\n", "&nbsp;")
        print "<a style='text-decoration: none' href=\"%s\" title=\"%s\">" % ("log/" + e['carid'] + "/" + e['hash'] + ".txt", titlemsg)
        print "<font color=gray>"
        print e['lastmsg']
        print "</font>"
        print "</a>"
        print "</span>"
    
    def PrintLogCar(self):
        carid = self.GetCarID()
        (fuels, accepted, sys, users, ours) = self.log.GetLog(carid)
        n = len(fuels)
        fuel_keys = sorted(fuels.keys())
        rows = (n+self.COLS-1)/self.COLS
        print "<div style='margin: 1em'>"
        print "<div style='margin: 1em 0em'>"
        print "<span style='font-size: large; font-weight: bold'>"
        print "<a name='%s'>" % carid
        if ours:
            print "<font color=green>%s</font>" % carid
        elif accepted:
            print "<font color=blue>%s</font>" % carid
        else:
            print "<font color=red>%s</font>" % carid
        print "</a>"
        print "</span>"
        print "<span>"
        print "<font color=black>%s users</font>" % users
        print "<font color=gray>(%d)</font>" % len(fuels)
        print "<a href='log/%s/SPEC'><font color=gray>spec</font></a>" % carid
        print "<a href='log/%s/SYSTEM'><font color=gray>sys</font></a>" % carid
        print "<a href='log/%s/SYSTEM2'><font color=gray>sys2</font></a>" % carid
        print "<a href='?action=submit&amp;carid=%s'><font color=gray>submit</font></a>" % carid
        print "</span>"
        print "<div style='padding-left: 1em'><pre>%s</pre></div>" % sys
        print "</div>"
        print "<table style='font-size: small' width='100%%'>"
        for row in range(rows):
            print "<tr>"
            for col in range(self.COLS):
                i = row + col*rows
                if i >= n:
                    continue
                e = fuels[fuel_keys[i]]
                print "<td width='%d%%' style='margin-right: 3px'>" % (100/self.COLS)
                self.PrintEntry(e)
                print "</td>"
            print "</tr>"
        print "</table></div>"

    def PrintLogList(self):
        only = self.form['only'].value if 'only' in self.form else ""
        sortopt = self.form['sort'].value if 'sort' in self.form else ""
        fuel_all = dict()
        for carid in self.log.GetCarIDs():
            fuel_all[carid] = self.log.GetLog(carid)
        num_solved = len([1 for x in fuel_all.values() if x[1]])
        num_unsolved = len([1 for x in fuel_all.values() if not x[1]])
        num_ours = len([1 for x in fuel_all.values() if x[4]])
        num_rare_solved = 0
        num_rare_unsolved = 0
        num_two_solved = 0
        num_two_unsolved = 0
        num_popular_unsolved = 0
        num_monopoly_unsolved = 0
        for (fuels, accepted, sys, users, ours) in fuel_all.values():
            if 3 <= int(users) <= 4:
                if accepted:
                    num_rare_solved += 1
                else:
                    num_rare_unsolved += 1
            if 2 == int(users):
                if accepted:
                    num_two_solved += 1
                else:
                    num_two_unsolved += 1
            if int(users) >= 5 and not accepted:
                num_popular_unsolved += 1
            if int(users) <= 1 and not accepted:
                num_monopoly_unsolved += 1
        def popularity_cmp(a, b):
            if fuel_all[a][3] != fuel_all[b][3]:
                return -cmp(int(fuel_all[a][3]), int(fuel_all[b][3]))
            return cmp(int(a), int(b))
        def num_cmp(a, b):
            return cmp(int(a), int(b))
        fuel_keys = list(fuel_all.keys())
        if sortopt == 'asc':
            fuel_keys.sort(num_cmp)
        elif sortopt == 'desc':
            fuel_keys.sort(num_cmp)
            fuel_keys.reverse()
        else:
            fuel_keys.sort(popularity_cmp)
        scraped_score = purepure.FileUtil.ReadFile("../status/SCORE").strip()
        scraped_solved = purepure.FileUtil.ReadFile("../status/SOLVED").strip()
        scraped_mycars = purepure.FileUtil.ReadFile("../status/MYCARS").strip()
        print '<div style="position: absolute; right: 1em; top: 1em; background-color: white; padding: 0.4em; border: 1px dotted #888;">'
        print 'Our Score: <b>%s</b><br>' % scraped_score
        print 'Our Solved: <b>%s</b><br>' % scraped_solved
        print 'Our Cars: <b>%s</b><br>' % scraped_mycars
        print '>=5 users: <b>%d</b> unsolved<br>' % num_popular_unsolved
        print '3-4 users: <b>%d</b> solved, <b>%d</b> unsolved<br>' % (num_rare_solved, num_rare_unsolved)
        print '2 users: <b>%d</b> solved, <b>%d</b> unsolved<br>' % (num_two_solved, num_two_unsolved)
        print 'Monopoly: <b>%d</b> unsolved<br>' % num_monopoly_unsolved
        print '<b>%d%%</b> of the Market are belong to us<br>' % (100 * num_solved / (num_solved + num_unsolved))
        print '</div>'
        print """
<div>
Show Only: 
<a href='?'>All (by popularity)</a>
<a href='?sort=asc'>All (by ID)</a>
<a href='?only=unsolved'>Unsolved (%d)</a>
<a href='?only=solved'>Solved (%d)</a>
<a href='?only=ours'>Ours (%d)</a>
<br>
For automation:
<a href='log/'>Raw Log</a>
<a href='?action=unsolved'>Unsolved IDs</a>
<a href='?action=solved'>Solved IDs</a>
</div>
""" % (num_unsolved, num_solved, num_ours)
        for carid in fuel_keys:
            (fuels, accepted, sys, users, ours) = fuel_all[carid]
            if only == 'unsolved' and accepted:
                continue
            if only == 'solved' and not accepted:
                continue
            if only == 'ours':
                if not ours:
                    continue
            elif ours:
                continue
            print "<div style='margin: 0.5em'>"
            print "<span style='font-size: normal; font-weight: bold'>"
            print "<a name='%s' href='?action=car&amp;carid=%s' style='text-decoration: none'>" % (carid, carid)
            if ours:
                print "<font color=green>%s</font>" % carid
            elif accepted:
                print "<font color=blue>%s</font>" % carid
            else:
                print "<font color=red>%s</font>" % carid
            print "</span></a>"
            print "<span>"
            print "<font color=black>%s users</font>" % users
            print "<font color=gray>(%d)</font>" % len(fuels)
            print "<a href='log/%s/SPEC'><font color=gray>spec</font></a>" % carid
            print "<a href='log/%s/SYSTEM'><font color=gray>sys</font></a>" % carid
            print "<a href='log/%s/SYSTEM2'><font color=gray>sys2</font></a>" % carid
            print "<a href='?action=submit&amp;carid=%s'><font color=gray>submit</font></a>" % carid
            print "</span>"
            if not accepted and not ours:
                print "<div style='padding-left: 1em'><pre>%s</pre></div>" % sys
            print "</div>"

    def PrintSubmitForm(self):
        carid = self.GetCarID()
        print """
<form action="submit.cgi" method="post">
CarID: %s<br>
<input type="hidden" name="carid" value="%s">
RAW Fuel:<br>
<textarea name="fuel" rows="10" cols="80"></textarea>
<br>
<input type="submit">
</form>
""" % (carid, carid)
        print """<pre>
# Input Format:
#
# The first line contains two integers M and N, which indicates the number
# of tanks and air components respectively.  Then M blocks follow; each block
# contains N lines consisting of N integers and represents a fuel operator.
#
# The input must be given from the standard input.  Each line can contain
# a comment starting with '#'.</pre>
"""
        print """
<form action="index.cgi" method="post">
<input type="hidden" name="carid" value="%s">
<input type="hidden" name="action" value="convert_submit">
Text Fuel:<br>
<textarea name="textfuel" rows="10" cols="80"></textarea>
<br>
<input type="submit">
</form>
""" % carid

    def ConvertAndRedirect(self):
        carid = self.GetCarID()
        textfuel = self.form['textfuel'].value
        old_stdin = sys.stdin
        try:
            sys.stdin = cStringIO.StringIO(textfuel)
            res = encfuel.encode(encfuel.scan())
        finally:
            sys.stdin = old_stdin
        print "Location: ./submit.cgi?carid=%s&fuel=%s" % (carid, res)
        print

    def PrintCarIDs(self, open):
        choice = self.form['choice'].value if 'choice' in self.form else ""
        cars_by_id = []
        for carid in self.log.GetCarIDs():
            e = self.log.GetLog(carid)
            cars_by_id.append((-int(carid), e, carid))
        cars_by_id.sort()
        n = len(cars_by_id)
        cars_by_score = []
        for i in range(n):
            base_score = int(cars_by_id[i][1][3])
            if base_score <= 0:
                base_score = 1
            score = base_score*100
            for d in range(-1, 2):
                if d == 0:
                    continue
                j = i + d
                if 0 <= j < n and cars_by_id[j][1][1]:
                    score += 1
            e = cars_by_id[i][1]
            carid = cars_by_id[i][2]
            cars_by_score.append((-score, random.random(), e, carid))
        cars_by_score.sort()
        for (score, seed, (users, accepted, sys, users, ours), carid) in cars_by_score:
            if accepted != open:
                print carid

    def PrintRegenerateList(self):
        fuel_all = dict()
        for carid in self.log.GetCarIDs():
            (fuels, accepted, sys, users, ours) = self.log.GetLog(carid)
            for fuel in fuels.values():
                if fuel['good']:
                    print carid, fuel['fuel']

    def PrintGrep(self):
        pat = self.form['pat'].value
        for carid in self.log.GrepCarSystem2(pat):
            print carid

    def PrintHtmlHeader(self):
        print "Content-Type: text/html"
        print ""
        print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">'
        print '<html>'
        print '<head>'
        print '<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">'
        print '</head>'
        print '<body>'
        print '<div style="position: absolute; right: 1em; top: 12em; width: 250px; height: 250px; background-image: url(image/purepurecode.png); background-repeat: no-repeat;"></div>'
        if DEBUG:
            print '<div style="position: absolute; top: 2em; width: 95%; text-align: center; z-index: -1">'
            print '<font color=red size=+3><b>TESTING</b></font>'
            print '</div>'

    def PrintTextHeader(self):
        print "Content-Type: text/plain"
        print ""

    def Main(self):

        self.form = cgi.FieldStorage()

        if 'action' in self.form:
            action = self.form['action'].value
        else:
            action = ""

        if action == "submit":
            self.PrintHtmlHeader()
            self.PrintSubmitForm()
        elif action == 'convert_submit':
            self.ConvertAndRedirect()
        elif action == 'car':
            self.PrintHtmlHeader()
            self.PrintLogCar()
        elif action == 'open' or action == 'unsolved':
            self.PrintTextHeader()
            self.PrintCarIDs(open=True)
        elif action == 'closed' or action == 'solved':
            self.PrintTextHeader()
            self.PrintCarIDs(open=False)
        elif action == 'regenerate_list':
            self.PrintTextHeader()
            self.PrintRegenerateList()
        elif action == 'grep':
            self.PrintTextHeader()
            self.PrintGrep()
        else:
            self.PrintHtmlHeader()
            self.PrintLogList()



def main():
    real_stdout = sys.stdout
    real_stderr = sys.stderr
    buffer = cStringIO.StringIO()
    sys.stdout = buffer
    sys.stderr = buffer
    try:
        os.chdir("log")
        web = PurePureWeb()
        web.Main()
        sys.stdout = real_stdout
        real_stdout.write(buffer.getvalue())
    except:
        sys.stdout = real_stdout
        sys.stderr = real_stdout
        print "Content-Type: text/plain"
        print ""
        print "Internal Server Error:"
        traceback.print_exc()
    finally:
        sys.stdout = real_stdout
        sys.stderr = real_stderr


if __name__ == '__main__':
    global DEBUG
    DEBUG = os.path.isfile("TESTING")
    if not DEBUG:
        main()
    else:
        import cProfile, pstats
        prof = cProfile.Profile()
        ctx = prof.runctx("main()", globals(), locals())
        stats = pstats.Stats(ctx, stream=sys.stdout)
        print "<h2>Profiler Data</h2><pre>"
        stats.sort_stats("cumulative")
        stats.print_stats(100)

