#!/bin/bash

cd /home/icfp/public_html/log

wget -q -O - "http://nfa.imn.htwk-leipzig.de/information/cars" | grep 'solved by:' | awk '{print $2 " " $6}' | while read carid users; do
    mkdir -p $carid
    echo "$users" > $carid/USERS
    echo "$carid: $users users"
    touch $carid
done
