#include <iostream>
#include <algorithm>
#include <sstream>
#include <vector>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <ctime>
using namespace std;

/*
typedef vector<double> vec;
typedef vector<vec> mat;
*/

typedef vector<double> mat;

template<int N>
void mmult_fix_inter(const double *__restrict a, const double *__restrict b, double *__restrict ret)
{
  for (int i=0; i<N; i++){
    for (int j=0; j<N; j++){
      for (int k=0; k<N; k++){
        ret[j+N*i]+=a[k+N*i]*b[j+N*k];
      }
    }
  }
}

template<int N>
mat mmult_fix(const mat &a, const mat &b)
{
  //int n=a.size();
  int n = N;
  mat ret(n*n);
  mmult_fix_inter<N>(&a[0], &b[0], &ret[0]);
  return ret;
}


mat mmult(int n, const mat &a, const mat &b)
{
  if(n == 1) return mmult_fix<1>(a, b);
  if(n == 2) return mmult_fix<2>(a, b);
  if(n == 3) return mmult_fix<3>(a, b);
  if(n == 4) return mmult_fix<4>(a, b);
  if(n == 5) return mmult_fix<5>(a, b);
  if(n == 6) return mmult_fix<6>(a, b);
  if(n == 7) return mmult_fix<7>(a, b);
  if(n == 8) return mmult_fix<8>(a, b);
  if(n == 12) return mmult_fix<12>(a, b);
  if(n == 16) return mmult_fix<16>(a, b);
  if(n == 20) return mmult_fix<20>(a, b);
  //int n=a.size();
  mat ret(n*n);
  for (int i=0; i<n; i++){
    for (int j=0; j<n; j++){
      for (int k=0; k<n; k++){
        ret[j+n*i]+=a[k+n*i]*b[j+n*k];
      }
    }
  }
  return ret;
}

mat mat_id(int d)
{
  mat ret(d*d);
  for (int i=0; i<d; i++)
    ret[i*(d+1)]=1;
  return ret;
}

struct chamber{
  double valid(int n, const vector<mat> &m) const {
    mat um=mat_id(n);
    for (int i=0; i<u.size(); i++)
      um=mmult(n, m[u[i]], um);
    mat dm=mat_id(n);
    for (int i=0; i<d.size(); i++)
      dm=mmult(n, m[d[i]], dm);
    
    double score = n*n;
    for (int i=0; i<n; i++){
      for (int j=0; j<n; j++){
        if (!(um[i*n+j]>=dm[i*n+j])){
          double x=dm[i*n+j]-um[i*n+j]+1;
	  //cout<<x<<", "<<(x/(x+10))<<endl;
          score-=atan(x) * 2 / M_PI;
        }
      }
    }

    if (!strict){
      if (!(um[0]>dm[0])){
	--score;
      }
    }

    return score;
  }

  vector<int> u, d;
  int strict;
};

int getListLen()
{
  char c=cin.get();
  if (c=='0') return 0;
  if (c=='1') return 1;

  assert(c=='2');
  c=cin.get();
  assert(c=='2');
  c=cin.get();

  if (c=='0') return 2;
  if (c=='1'){
    c=cin.get();
    return 3+c-'0';
  }

  assert(c=='2');
  c=cin.get();
  assert(c=='2');
  c=cin.get();

  if (c=='0'){
    char c0=cin.get();
    char c1=cin.get();
    return 6+(c0-'0')*3+(c1-'0');
  }
  if (c=='1'){
    c=cin.get();
    if (c=='0'){
      char c0=cin.get();
      char c1=cin.get();
      char c2=cin.get();
      return 15+(c0-'0')*9+(c1-'0')*3+(c2-'0');
    }
    if (c=='1'){
      char c0=cin.get();
      char c1=cin.get();
      char c2=cin.get();
      char c3=cin.get();
      return 42+(c0-'0')*27+(c1-'0')*9+(c2-'0')*3+(c3-'0');
    }
    assert(c=='2');
    char c0=cin.get();
    char c1=cin.get();
    char c2=cin.get();
    char c3=cin.get();
    char c4=cin.get();
    return 123+(c0-'0')*81+(c1-'0')*27+(c2-'0')*9+(c3-'0')*3+(c4-'0');
  }
  assert(c=='2');
  c=cin.get();
  assert(c=='2');

  c=cin.get();
  if (c=='0'){
    c=cin.get();
    if (c=='0'){
      c=cin.get();
      if (c=='0'){
        char c0=cin.get();
        char c1=cin.get();
        char c2=cin.get();
        char c3=cin.get();
        char c4=cin.get();
        char c5=cin.get();
        return 366+(c0-'0')*243+(c1-'0')*81+(c2-'0')*27+(c3-'0')*9+(c4-'0')*3+(c5-'0');
      }
      if (c=='1'){
        char c0=cin.get();
        char c1=cin.get();
        char c2=cin.get();
        char c3=cin.get();
        char c4=cin.get();
        char c5=cin.get();
        char c6=cin.get();
        return 366+243*3+(c0-'0')*243*3+(c1-'0')*81*3+(c2-'0')*27*3+(c3-'0')*9*3+(c4-'0')*3*3+(c5-'0')*3+(c6-'0');
      }
    }
  }

  abort();
}

int getInt()
{
  char c=cin.get();
  if (c=='0') return 0;
  if (c=='1'){
    char c0=cin.get();
    return 1+(c0-'0');
  }
  assert(c=='2');
  c=cin.get();
  assert(c=='2');
  c=cin.get();
  if (c=='0'){
    char c1=cin.get();
    char c0=cin.get();
    return 4+(c0-'0')+(c1-'0')*3;
  }
  if (c=='1'){
    c=cin.get();
    if (c=='0'){
      char c2=cin.get();
      char c1=cin.get();
      char c0=cin.get();
      return 13+(c0-'0')+(c1-'0')*3+(c2-'0')*9;
    }
    if (c=='1'){
      char c3=cin.get();
      char c2=cin.get();
      char c1=cin.get();
      char c0=cin.get();
      return 40+(c0-'0')+(c1-'0')*3+(c2-'0')*9+(c3-'0')*27;
    }
    assert(c=='2');

    char c4=cin.get();
    char c3=cin.get();
    char c2=cin.get();
    char c1=cin.get();
    char c0=cin.get();
    return 121+(c0-'0')+(c1-'0')*3+(c2-'0')*9+(c3-'0')*27*(c4-'0')*81;
  }
  abort();
}

double calc_score(int n, const vector<chamber> &cs, const vector<mat> &m)
{
  double ret=0;
  for (int i=0; i<cs.size(); i++)
    ret += cs[i].valid(n, m);
  return ret / (n * n);
}

void solve(const vector<chamber> &cs, int tank, int dim)
{
  cerr<<"*** "<<dim<<endl;

  vector<mat> m
    (tank, mat
     (dim * dim));

  for (int i=0; i<tank; i++){
    for (int j=0; j<dim; j++)
      for (int k=0; k<dim; k++)
        m[i][k+j*dim]=(rand()%10);
    m[i][0]++;
  }

  double cur_score=calc_score(dim, cs, m);
  cerr<<"init score: "<<cur_score<<" / "<<cs.size()<<endl;
  double best_score=cur_score;
  

  double temp;
  for (  int turn=0; turn <= 200000; turn++){
    //if(temp < 2.0) temp = 2.0;
    int state = (turn / 1000) % 10;
    if(state == 0) temp = 10.0; else temp = 0.01;
    if (turn%1000==0) cerr<<turn<<": "<<temp<<endl;
    

    if (cur_score==cs.size()){
    //    if (cur_score==14){
      // solved!!
      cerr<<"====="<<endl;
      cout<<tank<<" "<<dim<<endl;
      for (int i=0; i<tank; i++){
        for (int j=0; j<dim; j++){
          for (int k=0; k<dim; k++){
            cout<<m[i][k + dim*j]<<" ";
          }
          cout<<endl;
        }
        cout<<endl;
      }
      exit(0);
    }

    int ri=rand()%tank;
    int rj=rand()%dim;
    int rk=rand()%dim;
    //int to=m[ri][rj][rk]+(rand()%21-10);
    double r1 = rand() / (RAND_MAX + 1.0);
    double r2 = rand() / (RAND_MAX + 1.0);

    double scale = (dim == 1 ? 200.0 : 10.0);
    int to = (int)(rand() * scale / (RAND_MAX + 1.0));//(int)((sqrt(-2. * log (r1)) * cos ( 2 * M_PI * r2)) * 5);
    int bk = m[ri][rk + dim*rj];
    
    if(to == bk) continue;
    //cout << "DEBUG: " << to << endl;
    if(rj == 0 && rk == 0 && to == 0) continue;
    
    //int to=rand()%100;

    //if (to<0) continue;
    //if (to>=100) continue;
    //if (rj==0 && rk==0 && to==0) continue;

    m[ri][rk + dim*rj]=to;
    /*
    if(m[ri][rj + dim*rk] < 0) m[ri][rj + dim*rk] = 0;
    if(m[ri][rj + dim*rk] > 10) m[ri][rj + dim*rk] = 10;
    */
    double next_score=calc_score(dim, cs, m);
    //cout<<next_score<<" "<<temp<<endl;

    if(isnan(next_score)) continue;

    if (exp((double)(next_score-cur_score)/temp)>(rand()/(double)RAND_MAX)){
      cur_score=next_score;
      if (cur_score>best_score){
        best_score=cur_score;
        cerr<<cur_score<<endl;
      }
    }
    else{
      m[ri][rk + dim*rj]=bk;
    }
  }
}

int main(int argc, char* argv[])
{
  srand(time(NULL));
  vector<chamber> cs;
  int tanks=0;
  if(argc >= 2 && string(argv[1]) == "--system") { // ad-hoc
    string line;
    while(getline(cin, line)){
      if(line.empty()) break;
      chamber ch;
      istringstream is(line);
      char c;
      int mode = 0;
      int strict = 0;
      while(is >> c){
	if(c == 'I' || c == 'M' || c == '[' || c == ']') continue;
	if(c == '-' || c == '>'){ ++mode; continue; }
	if(c == '='){ strict = 1; continue; }// ! historical naming !
	if(isdigit(c)||isalpha(c)) {
	  if(mode == 2) break; 
	  int tankno = (c > '9' ? (int)(c - 'A') : (int)(c - '0'));
	  if(tanks <= tankno) tanks = tankno + 1;
	  if(mode == 0) ch.u.push_back(tankno);
	  else ch.d.push_back(tankno);
	  continue;
	}
      }
      reverse(ch.u.begin(), ch.u.end());
      reverse(ch.d.begin(), ch.d.end());
      ch.strict = strict;
      cs.push_back(ch);
    }
  }else{
    int n = getListLen();
    cs.resize(n);
    
    
    for (int i=0; i<n; i++){
      chamber c;
      int an=getListLen();
      for (int j=0; j<an; j++){
	int t=getInt();
	c.u.push_back(t);
	tanks=max(tanks, t+1);
      }
      c.strict=getInt();
      int bn=getListLen();
      for (int j=0; j<bn; j++){
	int t=getInt();
	c.d.push_back(t);
	tanks=max(tanks, t+1);
      }
      cs[i]=c;
    }
  }

  //solve(cs, tanks, 1);
  for (int i=1; i<=6; i++)
    solve(cs, tanks, i);
  for (;;)
    solve(cs, tanks, 6);

  return 0;
}
