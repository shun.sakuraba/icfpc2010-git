-- 49865 nado...
main = do
  l1 <- getLine
  l2 <- getLine
  l3 <- getLine
  l4 <- getLine
  l5 <- getLine
  
  let n1 = length l1 - 6 + 1
  let n2 = length l2 - 6 + 1
  
  putStrLn "6 2"
  putStrLn $ show (2^n1) ++ " 0"
  putStrLn $ "0 0"
  putStrLn ""
  putStrLn "2 0"
  putStrLn "0 0"
  putStrLn ""
  putStrLn $ show (2^n2) ++ " 0"
  putStrLn $ "0 0"
  putStrLn ""
  putStrLn "2 0"
  putStrLn "0 0"
  putStrLn ""
  putStrLn "5 3"
  putStrLn "5 3"
  putStrLn ""
  putStrLn "2 9"
  putStrLn "3 6"
