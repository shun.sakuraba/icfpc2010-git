#!/bin/bash

exec 2> /dev/null
read spec

for t in data/*; do
    if echo "$spec" | cat - $t | ../../tools/encfuel/validator.py | grep -q good > /dev/null; then
        cat $t
        exit 0
    fi
done

exit 1
