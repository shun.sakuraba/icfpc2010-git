#! /usr/bin/python

import car_parser
import sys

def prod(iterable):
    n = 1
    for x in iterable:
        n *= x
    return n

def main():
    car = car_parser.ParseCar(sys.stdin.readline().strip())

    for idx in xrange(64):
        p = [ 1 + ((idx >> j) & 1) for j in xrange(6) ]
        okay = True
        for upper, ismain, lower in car:
            up = prod(p[j] for j in upper)
            lo = prod(p[j] for j in lower)
            if up < lo or (ismain and up == lo):
                okay = False
                break
        if okay:
            print 6, 1, ' '.join(str(p[j]) for j in xrange(6))
            break

if __name__ == '__main__':
    main()
