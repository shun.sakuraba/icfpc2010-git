#!/bin/bash

if [ "$1" = "--long" ]; then
    ulimit -t 20
else
    ulimit -t 5
fi

../../circuit/car_parser.py | python converter.py | ./a.out
