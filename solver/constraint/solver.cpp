#include <iostream>
#include <algorithm>
#include <vector>
#include <sstream>
#include <string>

using namespace std;

int nmax;
vector<vector<int> > uppers;
vector<vector<int> > lowers;
vector<bool> strict;

bool solve_inter(vector<int> &fuel, int pos, int total)
{
  if(pos == nmax){
    if(total != 0) return false;

    // constraint check
    //cout << "check " << endl;

    /*
    for(size_t j = 0; j < pos; ++j){
      cout << fuel[j] << " ";
    }
    cout << endl;
    */

    for(size_t i = 0; i < uppers.size(); ++i){
      long long c1 = 1L, c2 = 1L;
      vector<int> &u = uppers[i];
      for(size_t j = 0; j < u.size(); ++j)
	c1 *= fuel[u[j]];

      vector<int> &l = lowers[i];
      for(size_t j = 0; j < l.size(); ++j)
	c2 *= fuel[l[j]];

      if(strict[i]){
	if(c1 <= c2) return false;
      }else{
	if(c1 < c2) return false;
      }
    }

    return true;
  }else{
    if(total < 0) return false;

    for(int k = 1; k <= total; ++k) {
      fuel[pos] = k;
      if(solve_inter(fuel, pos + 1, total - k))
	return true;
    }
    return false;
  }
}

void solve()
{
  vector<int> fuels(nmax); // represent as 6
  int total;
  
  for(int i = 1; /* */; ++i) {
    total = i;
    //cout << total << endl;
    if(solve_inter(fuels, 0, total)){
      cout << nmax << " " << 1 << endl;
      for(size_t j = 0; j < fuels.size(); ++j){
	cout << fuels[j] << " ";
      }
      cout << endl;
      return;
    }
  }
}


int main(void)
{
  string line;
  nmax = 1;
  while(getline(cin, line)){
    istringstream is(line);
    
    if(line.empty()){
      break;
    }
    
    vector<int> u, l;

    string token;
    while(is >> token && token[0] != '>') {
      istringstream ts(token);
      int n;
      ts >> n;
      nmax = max(nmax, n + 1);

      u.push_back(n);
    }
    strict.push_back(token == ">");

    while(is >> token) {
      istringstream ts(token);
      int n;
      ts >> n;
      nmax = max(nmax, n + 1);
      
      l.push_back(n);
    }

    /*
    for(size_t i = 0; i < u.size(); ++i)
      cout << u[i] << " ";
    cout << endl;
    for(size_t i = 0; i < l.size(); ++i)
      cout << l[i] << " ";
    cout << endl;
    */

    uppers.push_back(u);
    lowers.push_back(l);
  }

  solve();

  return 0;
}
