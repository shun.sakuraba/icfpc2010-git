#include <iostream>
#include <vector>
#include <algorithm>
#include <sstream>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <stdint.h>
using namespace std;

/* Period parameters */  
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

static uint32_t mt[N]; /* the array for the state vector  */
static int mti=N+1; /* mti==N+1 means mt[N] is not initialized */

/* initializes mt[N] with a seed */
void init_genrand(uint32_t s)
{
    mt[0]= s & 0xffffffffUL;
    for (mti=1; mti<N; mti++) {
        mt[mti] = 
	    (1812433253UL * (mt[mti-1] ^ (mt[mti-1] >> 30)) + mti); 
        /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
        /* In the previous versions, MSBs of the seed affect   */
        /* only MSBs of the array mt[].                        */
        /* 2002/01/09 modified by Makoto Matsumoto             */
        mt[mti] &= 0xffffffffUL;
        /* for >32 bit machines */
    }
}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
/* slight change for C++, 2004/2/26 */
void init_by_array(uint32_t init_key[], int key_length)
{
    int i, j, k;
    init_genrand(19650218UL);
    i=1; j=0;
    k = (N>key_length ? N : key_length);
    for (; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1664525UL))
          + init_key[j] + j; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++; j++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
        if (j>=key_length) j=0;
    }
    for (k=N-1; k; k--) {
        mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1566083941UL))
          - i; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++;
        if (i>=N) { mt[0] = mt[N-1]; i=1; }
    }

    mt[0] = 0x80000000UL; /* MSB is 1; assuring non-zero initial array */ 
}

/* generates a random number on [0,0xffffffff]-interval */
uint32_t genrand_int32(void)
{
    uint32_t y;
    static uint32_t mag01[2]={0x0UL, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (mti >= N) { /* generate N words at one time */
        int kk;

        if (mti == N+1)   /* if init_genrand() has not been called, */
            init_genrand(5489UL); /* a default initial seed is used */

        for (kk=0;kk<N-M;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        for (;kk<N-1;kk++) {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1UL];

        mti = 0;
    }
  
    y = mt[mti++];

    /* Tempering */
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);

    return y;
}

uint64_t genrand_int64()
{
  uint64_t a=genrand_int32();
  uint64_t b=genrand_int32();
  return (a<<32LL)|b;
}

/* generates a random number on [0,1]-real-interval */
double genrand_real1(void)
{
    return genrand_int32()*(1.0/4294967295.0); 
    /* divided by 2^32-1 */ 
}

/* generates a random number on [0,1)-real-interval */
double genrand_real2(void)
{
    return genrand_int32()*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on (0,1)-real-interval */
double genrand_real3(void)
{
    return (((double)genrand_int32()) + 0.5)*(1.0/4294967296.0); 
    /* divided by 2^32 */
}

/* generates a random number on [0,1) with 53-bit resolution*/
double genrand_res53(void) 
{ 
    unsigned long a=genrand_int32()>>5, b=genrand_int32()>>6; 
    return(a*67108864.0+b)*(1.0/9007199254740992.0); 
} 

//-----

typedef vector<long double> vec;
typedef vector<vec> mat;

void mmult(const mat &a, const mat &b, mat &c)
{
  int n=a.size();
  for (int i=0; i<n; i++){
    for (int j=0; j<n; j++){
      c[i][j]=0;
      for (int k=0; k<n; k++){
        c[i][j]+=a[i][k]*b[k][j];
      }
    }
  }
}

void mat_id(int d, mat &c)
{
  for (int i=0; i<d; i++)
    for (int j=0; j<d; j++)
      c[i][j]=0;
  for (int i=0; i<d; i++)
    c[i][i]=1;
}

struct chamber{
  bool valid(const vector<mat> &m) const {
    int n=m[0].size();
    mat um(n, vec(n)), dm(n, vec(n));
    mat umt(n, vec(n)), dmt(n, vec(n));
    mat_id(n, um);
    for (int i=0; i<u.size(); i++){
      mmult(m[u[i]], um, umt);
      swap(um, umt);
    }
    mat_id(n, dm);
    for (int i=0; i<d.size(); i++){
      mmult(m[d[i]], dm, dmt);
      swap(dm, dmt);
    }
    
    for (int i=0; i<n; i++){
      for (int j=0; j<n; j++){
        if (!(um[i][j]>=dm[i][j])){
          return false;
        }
      }
    }

    if (!strict){
      if (!(um[0][0]>dm[0][0])){
         return false;
      }
    }

    return true;
  }

  long double valid_score(const vector<mat> &m) const {
    int n=m[0].size();
    int n2=n*n;
    mat um(n, vec(n)), dm(n, vec(n));
    mat umt(n, vec(n)), dmt(n, vec(n));
    mat_id(n, um);
    for (int i=0; i<u.size(); i++){
      mmult(m[u[i]], um, umt);
      swap(um, umt);
    }
    mat_id(n, dm);
    for (int i=0; i<d.size(); i++){
      mmult(m[d[i]], dm, dmt);
      swap(dm, dmt);
    }

    long double pen=0;
    int penn=0;
    
    for (int i=0; i<n; i++){
      for (int j=0; j<n; j++){
        if (!(um[i][j]>=dm[i][j])){
          long double x=log(dm[i][j]-um[i][j]+1);
	  //cout<<x<<", "<<(x/(x+10))<<endl;
          pen+=x/(x+1);
          penn++;
        }
      }
    }

    if (!strict){
      if (!(um[0][0]>dm[0][0])){
         pen++;
         penn++;
      }
    }

    if (penn==0) return 1;

    return (long double)(n2+1-pen)/(n2+1);
  }

  vector<int> u, d;
  int strict;
};

int getListLen()
{
  char c=cin.get();
  if (c=='0') return 0;
  if (c=='1') return 1;

  assert(c=='2');
  c=cin.get();
  assert(c=='2');
  c=cin.get();

  if (c=='0') return 2;
  if (c=='1'){
    c=cin.get();
    return 3+c-'0';
  }

  assert(c=='2');
  c=cin.get();
  assert(c=='2');
  c=cin.get();

  if (c=='0'){
    char c0=cin.get();
    char c1=cin.get();
    return 6+(c0-'0')*3+(c1-'0');
  }
  if (c=='1'){
    c=cin.get();
    if (c=='0'){
      char c0=cin.get();
      char c1=cin.get();
      char c2=cin.get();
      return 15+(c0-'0')*9+(c1-'0')*3+(c2-'0');
    }
    if (c=='1'){
      char c0=cin.get();
      char c1=cin.get();
      char c2=cin.get();
      char c3=cin.get();
      return 42+(c0-'0')*27+(c1-'0')*9+(c2-'0')*3+(c3-'0');
    }
    assert(c=='2');
    char c0=cin.get();
    char c1=cin.get();
    char c2=cin.get();
    char c3=cin.get();
    char c4=cin.get();
    return 123+(c0-'0')*81+(c1-'0')*27+(c2-'0')*9+(c3-'0')*3+(c4-'0');
  }
  assert(c=='2');
  c=cin.get();
  assert(c=='2');

  c=cin.get();
  if (c=='0'){
    c=cin.get();
    if (c=='0'){
      c=cin.get();
      if (c=='0'){
        char c0=cin.get();
        char c1=cin.get();
        char c2=cin.get();
        char c3=cin.get();
        char c4=cin.get();
        char c5=cin.get();
        return 366+(c0-'0')*243+(c1-'0')*81+(c2-'0')*27+(c3-'0')*9+(c4-'0')*3+(c5-'0');
      }
      if (c=='1'){
        char c0=cin.get();
        char c1=cin.get();
        char c2=cin.get();
        char c3=cin.get();
        char c4=cin.get();
        char c5=cin.get();
        char c6=cin.get();
        return 366+243*3+(c0-'0')*243*3+(c1-'0')*81*3+(c2-'0')*27*3+(c3-'0')*9*3+(c4-'0')*3*3+(c5-'0')*3+(c6-'0');
      }
    }
  }

  abort();
}

int getInt()
{
  char c=cin.get();
  if (c=='0') return 0;
  if (c=='1'){
    char c0=cin.get();
    return 1+(c0-'0');
  }
  assert(c=='2');
  c=cin.get();
  assert(c=='2');
  c=cin.get();
  if (c=='0'){
    char c1=cin.get();
    char c0=cin.get();
    return 4+(c0-'0')+(c1-'0')*3;
  }
  if (c=='1'){
    c=cin.get();
    if (c=='0'){
      char c2=cin.get();
      char c1=cin.get();
      char c0=cin.get();
      return 13+(c0-'0')+(c1-'0')*3+(c2-'0')*9;
    }
    if (c=='1'){
      char c3=cin.get();
      char c2=cin.get();
      char c1=cin.get();
      char c0=cin.get();
      return 40+(c0-'0')+(c1-'0')*3+(c2-'0')*9+(c3-'0')*27;
    }
    assert(c=='2');

    char c4=cin.get();
    char c3=cin.get();
    char c2=cin.get();
    char c1=cin.get();
    char c0=cin.get();
    return 121+(c0-'0')+(c1-'0')*3+(c2-'0')*9+(c3-'0')*27*(c4-'0')*81;
  }
  abort();
}

long double calc_score(const vector<chamber> &cs, const vector<mat> &m)
{
  long double ret=0;
  for (int i=0; i<cs.size(); i++)
    ret+=cs[i].valid_score(m);
  return ret;
}

void solve(const vector<chamber> &cs, int tank, int dim)
{
  init_genrand(time(NULL));

  cerr<<"*** "<<dim<<endl;

  long long num_limit=100;

  vector<mat> m
    (tank, mat
     (dim, vec(dim, 0)));

  for (int i=0; i<tank; i++){
    for (int j=0; j<dim; j++)
      for (int k=0; k<dim; k++)
        m[i][j][k]=(genrand_int64()%num_limit);
    m[i][0][0]++;
    /*
    for (int j=0; j<dim; j++)
      for (int k=0; k<dim; k++)
        m[i][j][k]=(j==k?1:0);
    */
  }

  long double cur_score=calc_score(cs, m);
  cerr<<"init score: "<<cur_score<<" / "<<cs.size()<<endl;
  long double best_score=cur_score;
  
  int turn=0;
  for (long double temp=cs.size(); temp>=0.000001; temp*=0.99999, turn++){
    if (turn%10000==0) cerr<<turn<<": "<<temp<<endl;

    /*
    if (temp<0.000002){
      best_score=0;
      temp=10;
    }
    */

    if (cur_score>=cs.size()-1e-10){
      // solved!!
      cerr<<"====="<<endl;

      cerr<<tank<<" "<<dim<<endl;
      for (int i=0; i<tank; i++){
        for (int j=0; j<dim; j++){
          for (int k=0; k<dim; k++){
            cerr<<m[i][j][k]<<" ";
          }
          cerr<<endl;
        }
        cerr<<endl;
      }

      cout<<tank<<" "<<dim<<endl;
      for (int i=0; i<tank; i++){
        for (int j=0; j<dim; j++){
          for (int k=0; k<dim; k++){
            cout<<m[i][j][k]<<" ";
          }
          cout<<endl;
        }
        cout<<endl;
      }
      exit(0);
    }

  _retry:
    int ri=genrand_int32()%tank;
    int rj=genrand_int32()%dim;
    int rk=genrand_int32()%dim;
    long long to=0;
    int ty=genrand_int32();
    if (ty%5==0) to=m[ri][rj][rk]+((int)(genrand_int32()%21)-10);
    else if (ty%5==1) to=0;
    else if (ty%5==2) to=1;
    else if (ty%5==3) to=genrand_int64()%num_limit;//(long long)exp(genrand_real2()*20); //genrand_int64()%num_limit;
    else to=genrand_int64()%10;

    if (to<0) goto _retry;
    if (to>=num_limit) goto _retry;
    if (rj==0 && rk==0 && to==0) goto _retry;

    long long bk=m[ri][rj][rk];

    m[ri][rj][rk]=to;
    long double next_score=calc_score(cs, m);
    //cerr<<next_score<<" "<<temp<<endl;

    if (exp((long double)(next_score-cur_score)/temp)>genrand_real2()){
      cur_score=next_score;
      if (cur_score>best_score){
        best_score=cur_score;

	/*	
	cerr<<"====="<<endl;
	
	cerr<<tank<<" "<<dim<<endl;
	for (int i=0; i<tank; i++){
	  for (int j=0; j<dim; j++){
	    for (int k=0; k<dim; k++){
	      cerr<<m[i][j][k]<<" ";
	    }
	    cerr<<endl;
	  }
	  cerr<<endl;
	}
	*/

        cerr<<cur_score<<endl;
      }
    }
    else{
      m[ri][rj][rk]=bk;
    }
  }
}

int main(int argc, char* argv[])
{
  vector<chamber> cs;
  int tanks=0;
  if(argc >= 2 && string(argv[1]) == "--system") { // ad-hoc
    string line;
    while(getline(cin, line)){
      if(line.empty()) break;
      chamber ch;
      istringstream is(line);
      char c;
      int mode = 0;
      int strict = 0;
      while(is >> c){
	if(c == 'I' || c == 'M' || c == '[' || c == ']') continue;
	if(c == '-' || c == '>'){ ++mode; continue; }
	if(c == '='){ strict = 1; continue; }// ! historical naming !
	if(isdigit(c)) {
	  if(mode == 2) break; 
	  int tankno = (int)(c - '0');
	  if(tanks <= tankno) tanks = tankno + 1;
	  if(mode == 0) ch.u.push_back(tankno);
	  else ch.d.push_back(tankno);
	  continue;
	}
      }
      reverse(ch.u.begin(), ch.u.end());
      reverse(ch.d.begin(), ch.d.end());
      ch.strict = strict;
      cs.push_back(ch);
    }
  }else{
    int n = getListLen();
    cs.resize(n);
    
    
    for (int i=0; i<n; i++){
      chamber c;
      int an=getListLen();
      for (int j=0; j<an; j++){
	int t=getInt();
	c.u.push_back(t);
	tanks=max(tanks, t+1);
      }
      c.strict=getInt();
      int bn=getListLen();
      for (int j=0; j<bn; j++){
	int t=getInt();
	c.d.push_back(t);
	tanks=max(tanks, t+1);
      }
      cs[i]=c;
    }
  }

  for (int i=2; ; i++)
    solve(cs, tanks, i);

  return 0;
}
