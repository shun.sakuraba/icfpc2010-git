#include <iostream>
#include <vector>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <ctime>
using namespace std;

typedef vector<double> vec;
typedef vector<vec> mat;

mat mmult(const mat &a, const mat &b)
{
  int n=a.size();
  mat ret(n, vec(n));
  for (int i=0; i<n; i++){
    for (int j=0; j<n; j++){
      for (int k=0; k<n; k++){
        ret[i][j]+=a[i][k]*b[k][j];
      }
    }
  }
  return ret;
}

mat mat_id(int d)
{
  mat ret(d, vec(d, 0));
  for (int i=0; i<d; i++)
    ret[i][i]=1;
  return ret;
}

struct chamber{
  int valid(const vector<mat> &m) const {
    int n=m[0].size();
    mat um=mat_id(n);
    for (int i=0; i<u.size(); i++)
      um=mmult(m[u[i]], um);
    mat dm=mat_id(n);
    for (int i=0; i<d.size(); i++)
      dm=mmult(m[d[i]], dm);
    
    int score = 0;
    for (int i=0; i<n; i++){
      for (int j=0; j<n; j++){
	score += ((strict && um[i][j] - dm[i][j] > -0.5) || (um[i][j] - dm[i][j] > 0.5));
      }
    }

    return score;
  }

  vector<int> u, d;
  int strict;
};

int getListLen()
{
  char c=cin.get();
  if (c=='0') return 0;
  if (c=='1') return 1;

  assert(c=='2');
  c=cin.get();
  assert(c=='2');
  c=cin.get();

  if (c=='0') return 2;
  if (c=='1'){
    c=cin.get();
    return 3+c-'0';
  }

  assert(c=='2');
  c=cin.get();
  assert(c=='2');
  c=cin.get();

  if (c=='0'){
    char c0=cin.get();
    char c1=cin.get();
    return 6+(c0-'0')*3+(c1-'0');
  }
  if (c=='1'){
    c=cin.get();
    if (c=='0'){
      char c0=cin.get();
      char c1=cin.get();
      char c2=cin.get();
      return 15+(c0-'0')*9+(c1-'0')*3+(c2-'0');
    }
    if (c=='1'){
      char c0=cin.get();
      char c1=cin.get();
      char c2=cin.get();
      char c3=cin.get();
      return 42+(c0-'0')*27+(c1-'0')*9+(c2-'0')*3+(c3-'0');
    }
    assert(c=='2');
    char c0=cin.get();
    char c1=cin.get();
    char c2=cin.get();
    char c3=cin.get();
    char c4=cin.get();
    return 123+(c0-'0')*81+(c1-'0')*27+(c2-'0')*9+(c3-'0')*3+(c4-'0');
  }
  assert(c=='2');
  c=cin.get();
  assert(c=='2');

  c=cin.get();
  if (c=='0'){
    c=cin.get();
    if (c=='0'){
      c=cin.get();
      if (c=='0'){
        char c0=cin.get();
        char c1=cin.get();
        char c2=cin.get();
        char c3=cin.get();
        char c4=cin.get();
        char c6=cin.get();
        return 366+(c0-'0')*243+(c1-'0')*81+(c2-'0')*27+(c3-'0')*9+(c4-'0')*3+(c5-'0');
      }
    }
  }

  abort();
}

int getInt()
{
  char c=cin.get();
  if (c=='0') return 0;
  if (c=='1'){
    char c0=cin.get();
    return 1+(c0-'0');
  }
  assert(c=='2');
  c=cin.get();
  assert(c=='2');
  c=cin.get();
  if (c=='0'){
    char c1=cin.get();
    char c0=cin.get();
    return 4+(c0-'0')+(c1-'0')*3;
  }
  if (c=='1'){
    c=cin.get();
    if (c=='0'){
      char c2=cin.get();
      char c1=cin.get();
      char c0=cin.get();
      return 13+(c0-'0')+(c1-'0')*3+(c2-'0')*9;
    }
    if (c=='1'){
      char c3=cin.get();
      char c2=cin.get();
      char c1=cin.get();
      char c0=cin.get();
      return 40+(c0-'0')+(c1-'0')*3+(c2-'0')*9+(c3-'0')*27;
    }
    assert(c=='2');

    char c4=cin.get();
    char c3=cin.get();
    char c2=cin.get();
    char c1=cin.get();
    char c0=cin.get();
    return 121+(c0-'0')+(c1-'0')*3+(c2-'0')*9+(c3-'0')*27*(c4-'0')*81;
  }
  abort();
}

int calc_score(const vector<chamber> &cs, const vector<mat> &m)
{
  int ret=0;
  for (int i=0; i<cs.size(); i++)
    ret += cs[i].valid(m);
  return ret;
}

void solve(const vector<chamber> &cs, int tank, int dim)
{
  cout<<"*** "<<dim<<endl;

  vector<mat> m
    (tank, mat
     (dim, vec(dim, 0)));

  for (int i=0; i<tank; i++){
    for (int j=0; j<dim; j++)
      for (int k=0; k<dim; k++)
        m[i][j][k]=(rand()%100);
    m[i][0][0]++;
  }

  int cur_score=calc_score(cs, m);
  cout<<"init score: "<<cur_score<<" / "<<cs.size() * dim * dim<<endl;
  int best_score=cur_score;
  
  int turn=0;
  for (double temp=10; temp>=0.1; temp*=0.999995, turn++){
    if (turn%10000==0) cout<<turn<<": "<<temp<<endl;

    if (cur_score==cs.size()*dim*dim){
      // solved!!
      cout<<"====="<<endl;
      cout<<tank<<" "<<dim<<endl;
      for (int i=0; i<tank; i++){
        for (int j=0; j<dim; j++){
          for (int k=0; k<dim; k++){
            cout<<m[i][j][k]<<" ";
          }
          cout<<endl;
        }
        cout<<endl;
      }
      exit(0);
    }

    int ri=rand()%tank;
    int rj=rand()%dim;
    int rk=rand()%dim;
    //int to=m[ri][rj][rk]+(rand()%21-10);
    double r1 = rand() / (RAND_MAX + 1.0);
    double r2 = rand() / (RAND_MAX + 1.0);

    int to = (int)(abs(sqrt(-2. * log (r1)) * cos ( 2 * M_PI * r2)) * 10);
    //int to=rand()%100;

    if (to<0) continue;
    //if (to>=100) continue;
    if (rj==0 && rk==0 && to==0) continue;

    int bk=m[ri][rj][rk];

    m[ri][rj][rk]=to;
    int next_score=calc_score(cs, m);
    //cout<<next_score<<" "<<temp<<endl;

    if (exp((double)(next_score-cur_score)/temp)>(rand()/(double)RAND_MAX)){
      cur_score=next_score;
      if (cur_score>best_score){
        best_score=cur_score;
        cout<<cur_score<<endl;
      }
    }
    else{
      m[ri][rj][rk]=bk;
    }
  }
}

int main()
{
  srand(time(NULL));
  int n = getListLen();
  vector<chamber> cs(n);

  int tanks=0;

  for (int i=0; i<n; i++){
    chamber c;
    int an=getListLen();
    for (int j=0; j<an; j++){
      int t=getInt();
      c.u.push_back(t);
      tanks=max(tanks, t+1);
    }
    c.strict=getInt();
    int bn=getListLen();
    for (int j=0; j<bn; j++){
      int t=getInt();
      c.d.push_back(t);
      tanks=max(tanks, t+1);
    }
    cs[i]=c;
  }

  for (int i=2; ; i++)
    solve(cs, tanks, i);

  return 0;
}
