#!/usr/bin/python
#
# Solver for
#  M[X] - M[Y]^(K) > 0
#  M[Y]^(K) - M[X] >= 0
#  M[X1]...M[XN] - M[X1]...M[XN] >= 0
# Style problem.

import sys

def ParsePrefix(is_length, line):
    if line[0] == '0':
        return (0, line[1:])
    if line[0] == '1':
        if is_length:
            return (1, line[1:])
        else:
            return (1 + int(line[1]), line[2:])
    assert line[0:2] == '22', line
    n, line = ParsePrefix(is_length, line[2:])
    if not is_length:
        n += 2
    value = (3 ** n - 1) / 2
    if n != 0:
        value += int(line[0:n], 3)
    if is_length:
        value += 2
    return (value, line[n:])

def ParseLength(line):
    return ParsePrefix(True, line)

def ParseValue(line):
    return ParsePrefix(False, line)

def ParsePipe(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (value, line) = ParseValue(line)
        result.append(value)
    return (result, line)
    
def ParseChamber(line):
    (upper, line) = ParsePipe(line)
    (flg, line) = ParseValue(line)
    (lower, line) = ParsePipe(line)
    return ((upper, flg == 0, lower), line)

def ParseCar(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (chamber, line) = ParseChamber(line)
        result.append(chamber)

    # All characters should be used.
    assert line == '', line;
    return result

def CountSection(pipe):
    mp = { }
    for section in pipe:
        mp[section] = mp.setdefault(section, 0) + 1
    return mp

def Check(car, solution):
    if solution is None: return False

    for (upper, flg, lower) in car:
        uv = 1
        for x in upper:
            uv *= solution[x]
        lv = 1
        for x in lower:
            lv += solution[x]
        if uv < lv or (flg and uv == lv):
            return False
    return True
    
def Solve(car):
    solution = {}
    for (upper, flg, lower) in car:
        um = CountSection(upper)
        lm = CountSection(lower)
        if um == lm:
            if flg: return None
        elif len(um) == len(lm) == 1:
            uindex, uvalue = um.popitem()
            lindex, lvalue = lm.popitem()
            if uindex == lindex: return None
            if uvalue != 1 and lvalue != 1: return None
            if uvalue == 1:
                if uindex in solution:
                    if solution[uindex] == 2: return None
                else:
                    solution[uindex] = (2 ** lvalue) + (1 if flg else 0)
                v = solution.setdefault(lindex, 2)
                if v != 2: return None
            else:
                # lvalue == 1
                if lindex in solution:
                    if solution[lindex] == 2: return None
                else:
                    solution[lindex] = (2 ** uvalue) - (1 if flg else 0)
                v = solution.setdefault(uindex, 2)
                if v != 2: return None
        else:
            return None
    return solution

def Print(solution):
    length = len(solution)
    print "%d %d" % (length, 1)
    for index in xrange(length):
        print str(solution[index])

def main(args):
    input = raw_input()
    car = ParseCar(input)
    solution = Solve(car)
    assert Check(car, solution), solution
    Print(solution)

if __name__ == "__main__":
    main(sys.argv)
