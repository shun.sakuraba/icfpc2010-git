#!/usr/bin/python

import sys
import re
import cStringIO as StringIO

def ParsePrefix(is_length, line):
    if line[0] == '0':
        return (0, line[1:])
    if line[0] == '1':
        if is_length:
            return (1, line[1:])
        else:
            return (1 + int(line[1]), line[2:])
    assert line[0:2] == '22', line
    n, line = ParsePrefix(is_length, line[2:])
    if not is_length:
        n += 2
    value = (3 ** n - 1) / 2
    if n != 0:
        value += int(line[0:n], 3)
    if is_length:
        value += 2
    return (value, line[n:])

def ParseLength(line):
    return ParsePrefix(True, line)

def ParseValue(line):
    return ParsePrefix(False, line)

def ParsePipe(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (value, line) = ParseValue(line)
        result.append(value)
    return (result, line)
    
def ParseChamber(line):
    (upper, line) = ParsePipe(line)
    (flg, line) = ParseValue(line)
    (lower, line) = ParsePipe(line)
    return ((upper, flg == 0, lower), line)

def ParseCar(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (chamber, line) = ParseChamber(line)
        result.append(chamber)

    # All characters should be used.
    assert line == '', line;
    return result

def PipeToStr(pipe):
    return ''.join(reversed([chr(0x41 + section) for section in pipe]))

def CountTanks(car):
    tanks = set()
    for upper, flg, lower in car:
        for v in upper:
            tanks.add(v)
        for v in lower:
            tanks.add(v)
    return len(tanks)

def MatchPattern(car):
    return CountTanks(car) <= 2

def Evaluate(solution, pipe):
    result = 1
    for section in reversed(pipe):
        result = result * solution[section]

    return result

def Solve(car):
    if CountTanks(car) == 1:
        for v in xrange(1, 10000):
            solution = { 0: v }
            if Check(car, solution):
                return solution
        return None

    for total in xrange(2, 100):
        for v1 in xrange(1, total):
            v2 = total - v1
            solution = { 0: v1, 1: v2 }
            if Check(car, solution):
                return solution

    return None

def Check(car, solution):
    for (upper, flg, lower) in car:
        uv = Evaluate(solution, upper)
        lv = Evaluate(solution, lower)
        if flg:
            if not (uv - lv > 0): return False
        else:
            if not (uv - lv >= 0): return False

    return True

def GiveUp():
    sys.exit(1)

def PrintSolution(solution):
    print '%d 1' % (len(solution))

    for index in xrange(len(solution)):
        v = solution[index]
        print v

def main(args):
    data = raw_input()
    car = ParseCar(data)
    if not MatchPattern(car):
        print >>sys.stderr, 'Failed to match pattern'
        GiveUp()

    solution = Solve(car)
    if solution is None:
        print >>sys.stderr, 'Failed to solve'
        GiveUp()

    if not (Check(car, solution)):
        print >>sys.stderr, 'Failed to check'
        GiveUp()

    PrintSolution(solution)

if __name__ == '__main__':
    main(sys.argv)


def main():
    pass


if __name__ == '__main__':
    main()
