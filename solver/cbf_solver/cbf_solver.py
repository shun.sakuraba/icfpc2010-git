#!/usr/bin/python

import sys
import re
import cStringIO as StringIO

def ParsePrefix(is_length, line):
    if line[0] == '0':
        return (0, line[1:])
    if line[0] == '1':
        if is_length:
            return (1, line[1:])
        else:
            return (1 + int(line[1]), line[2:])
    assert line[0:2] == '22', line
    n, line = ParsePrefix(is_length, line[2:])
    if not is_length:
        n += 2
    value = (3 ** n - 1) / 2
    if n != 0:
        value += int(line[0:n], 3)
    if is_length:
        value += 2
    return (value, line[n:])

def ParseLength(line):
    return ParsePrefix(True, line)

def ParseValue(line):
    return ParsePrefix(False, line)

def ParsePipe(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (value, line) = ParseValue(line)
        result.append(value)
    return (result, line)
    
def ParseChamber(line):
    (upper, line) = ParsePipe(line)
    (flg, line) = ParseValue(line)
    (lower, line) = ParsePipe(line)
    return ((upper, flg == 0, lower), line)

def ParseCar(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (chamber, line) = ParseChamber(line)
        result.append(chamber)

    # All characters should be used.
    assert line == '', line;
    return result


def PipeToStr(pipe):
    return ''.join(reversed([chr(0x41 + section) for section in pipe]))

def MatchPattern(car):
    PATTERN = [
        'CBDCBA-(CBF)+(CBE)+ >= 0',
        'CBECBECBF-CBFCBFCBE > 0',
        '(CBF)+(CBE)+-CBDCBA >= 0',
    ]

    expr_list = ['%s-%s %s 0' % (PipeToStr(upper), PipeToStr(lower), '>' if flg else '>=')
                 for (upper, flg, lower) in car]

    for ptn in PATTERN:
        re_ptn = re.compile(ptn)

        for index,expr in enumerate(expr_list):
            if re_ptn.match(expr):
                del expr_list[index]
                break
        else:
            print >>sys.stderr, 'Not found: %s' % ptn
            return False
    if len(expr_list) > 0:
        return False

    chamber1 = car[0]
    chamber2 = car[2]
    return chamber1[2] == chamber2[0]

class Mat(object):
    def __init__(self, m11, m12, m21, m22):
        self.m11 = m11
        self.m12 = m12
        self.m21 = m21
        self.m22 = m22

    # Return self * other
    def mult(self, other):
        return Mat(self.m11 * other.m11 + self.m12 * other.m21,
                   self.m11 * other.m12 + self.m12 * other.m22,
                   self.m21 * other.m11 + self.m22 * other.m21,
                   self.m21 * other.m12 + self.m22 * other.m22)

def FindFE(car):
    for (upper, flg, lower) in car:
        if upper == [4, 5] and not flg and lower[-1] == 0 and lower[0] == 1:
            return (upper, flg, lower)
    return None

def CreatePrimeList(huge_val):
    result = [2]
    candidate = 3
    while candidate * candidate <= huge_val:
        for prime in result:
            if candidate % prime == 0: break
        else:
            result.append(candidate)
        candidate += 2
    return result

def Evaluate(solution, pipe):
    result = 1
    for section in reversed(pipe):
        result = result * solution[section]

    return result

def Solve(car):
    # find FE - A(DC+)+B >= 0
    A = 1
    B = 1
    C = 1
    # D
    E = 2
    F = 1
    
    solution = {
        0: A,
        1: B,
        2: C,
        4: E,
        5: F,
    }

    # Solve D
    D = Evaluate(solution, car[0][2])

    solution[3] = D
    return solution

def Check(car, solution):
    for (upper, flg, lower) in car:
        uv = Evaluate(solution, upper)
        lv = Evaluate(solution, lower)
        if flg:
            if not (uv - lv > 0): return False
        else:
            if not (uv - lv >= 0): return False

    return True

def GiveUp():
    sys.exit(1)

def PrintSolution(solution):
    print '6 1'

    for index in xrange(6):
        v = solution[index]
        print v

def main(args):
    data = raw_input()
    car = ParseCar(data)
    if not MatchPattern(car):
        print >>sys.stderr, 'Failed to match pattern'
        GiveUp()

    solution = Solve(car)
    if solution is None:
        print >>sys.stderr, 'Failed to solve'
        GiveUp()

    if not (Check(car, solution)):
        print >>sys.stderr, 'Failed to check'
        GiveUp()

    PrintSolution(solution)

if __name__ == '__main__':
    main(sys.argv)


def main():
    pass


if __name__ == '__main__':
    main()
