#!/usr/bin/python

import sys
import re
import cStringIO as StringIO

def permutations(iterable, r=None):
    # permutations('ABCD', 2) --> AB AC AD BA BC BD CA CB CD DA DB DC
    # permutations(range(3)) --> 012 021 102 120 201 210
    pool = tuple(iterable)
    n = len(pool)
    r = n if r is None else r
    if r > n:
        return
    indices = range(n)
    cycles = range(n, n-r, -1)
    yield tuple(pool[i] for i in indices[:r])
    while n:
        for i in reversed(range(r)):
            cycles[i] -= 1
            if cycles[i] == 0:
                indices[i:] = indices[i+1:] + indices[i:i+1]
                cycles[i] = n - i
            else:
                j = cycles[i]
                indices[i], indices[-j] = indices[-j], indices[i]
                yield tuple(pool[i] for i in indices[:r])
                break
        else:
            return

def ParsePrefix(is_length, line):
    if line[0] == '0':
        return (0, line[1:])
    if line[0] == '1':
        if is_length:
            return (1, line[1:])
        else:
            return (1 + int(line[1]), line[2:])
    assert line[0:2] == '22', line
    n, line = ParsePrefix(is_length, line[2:])
    if not is_length:
        n += 2
    value = (3 ** n - 1) / 2
    if n != 0:
        value += int(line[0:n], 3)
    if is_length:
        value += 2
    return (value, line[n:])

def ParseLength(line):
    return ParsePrefix(True, line)

def ParseValue(line):
    return ParsePrefix(False, line)

def ParsePipe(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (value, line) = ParseValue(line)
        result.append(value)
    return (result, line)
    
def ParseChamber(line):
    (upper, line) = ParsePipe(line)
    (flg, line) = ParseValue(line)
    (lower, line) = ParsePipe(line)
    return ((upper, flg == 0, lower), line)

def ParseCar(line):
    (size, line) = ParseLength(line)
    result = []
    for i in xrange(size):
        (chamber, line) = ParseChamber(line)
        result.append(chamber)

    # All characters should be used.
    assert line == '', line;
    return result


def PipeToStr(pipe):
    return ''.join(reversed([chr(0x41 + section) for section in pipe]))


def GenerateGroups():
    result = []
    for group in permutations('ABCDEF'):
        group1 = group[0:3]
        group2 = group[3:]
        if group1 < group2:
            result.append((group[0:3], group[3:]))
    return result

def GeneratePattern(group):
    return ['%s-%s+ >= 0' % (group[0], group[1]),
            '%s+-%s >= 0' % (group[1], group[0]),
            '%s-%s+ >= 0' % (group[1], group[2]),
            '%s+-%s >= 0' % (group[2], group[1])]
        

def GeneratePatternList():
    ptns_list = []
    for group1, group2 in GenerateGroups():
        ptns = []
        ptns.extend(GeneratePattern(group1))
        ptns.extend(GeneratePattern(group2))
        ptns.append('[%s]+-[%s]+ > 0' % (group1, group2))
        ptns.append('[%s]+-[%s]+ > 0' % (group2, group1))

        ptns_list.append((ptns, (group1, group2)))

    return ptns_list

def MatchPattern(car):
    ptns_list = GeneratePatternList()
    orig_expr_list = ['%s-%s %s 0' % (PipeToStr(upper), PipeToStr(lower), '>' if flg else '>=')
                 for (upper, flg, lower) in car]

    for ptns, groups in ptns_list:
        expr_list = orig_expr_list[:]

        for ptn in ptns:
            re_ptn = re.compile(ptn)

            for index,expr in enumerate(expr_list):
                if re_ptn.match(expr):
                    del expr_list[index]
                    break
            else:
                print >>sys.stderr, 'Not found: %s' % ptn
                break

        if len(expr_list) == 0:
            return (ptns, groups)

    return None

class Mat(object):
    def __init__(self, m11, m12, m21, m22):
        self.m11 = m11
        self.m12 = m12
        self.m21 = m21
        self.m22 = m22

    # Return self * other
    def mult(self, other):
        return Mat(self.m11 * other.m11 + self.m12 * other.m21,
                   self.m11 * other.m12 + self.m12 * other.m22,
                   self.m21 * other.m11 + self.m22 * other.m21,
                   self.m21 * other.m12 + self.m22 * other.m22)

def FindFE(car):
    for (upper, flg, lower) in car:
        if upper == [4, 5] and not flg and lower[-1] == 0 and lower[0] == 1:
            return (upper, flg, lower)
    return None

def CreatePrimeList(huge_val):
    result = [2]
    candidate = 3
    while candidate * candidate <= huge_val:
        for prime in result:
            if candidate % prime == 0: break
        else:
            result.append(candidate)
        candidate += 2
    return result

def Evaluate(solution, pipe):
    result = 1
    for section in reversed(pipe):
        result = result * solution[section]

    return result

def CalcExp(car, var1, var2):
    v1 = ord(var1) - 0x41
    v2 = ord(var2) - 0x41

    for (upper, flg, lower) in car:
        if len(upper) != 1 or upper[0] != v1 or any(v != v2 for v in lower): continue
        return len(lower)

    return -1

def Solve(car, group1, group2):
    # Group1 [1>2>3]
    exp12 = CalcExp(car, group1[1], group1[2])
    exp11 = CalcExp(car, group1[0], group1[1]) * exp12

    # Group2
    exp22 = CalcExp(car, group2[1], group2[2])
    exp21 = CalcExp(car, group2[0], group2[1]) * exp22

    # G1^x - G2^y > 0
    # G2^z - G1^w > 0

    # find x, y
    for (upper, flg, lower) in car:
        if not flg: continue
        if chr(0x41 + upper[0]) not in group1: continue
        x = (upper.count(ord(group1[0]) - 0x41) * exp11
             + upper.count(ord(group1[1]) - 0x41) * exp12
             + upper.count(ord(group1[2]) - 0x41))
        y = (lower.count(ord(group2[0]) - 0x41) * exp21
             + lower.count(ord(group2[1]) - 0x41) * exp22
             + lower.count(ord(group2[2]) - 0x41))
        break

    # find z w
    for (upper, flg, lower) in car:
        if not flg: continue
        if chr(0x41 + upper[0]) not in group2: continue
        print >>sys.stderr, upper
        print >>sys.stderr, lower
        z = (upper.count(ord(group2[0]) - 0x41) * exp21
             + upper.count(ord(group2[1]) - 0x41) * exp22
             + upper.count(ord(group2[2]) - 0x41))
        w = (lower.count(ord(group1[0]) - 0x41) * exp11
             + lower.count(ord(group1[1]) - 0x41) * exp12
             + lower.count(ord(group1[2]) - 0x41))
        break

    print >>sys.stderr, 'x: %d, y: %d, z:%d, w:%d' % (x, y, z, w)

    for sum in xrange(4, 10000):
#        print >>sys.stderr, 'trial: %d' % sum
        for G1 in xrange(2, sum):
            G2 = sum - G1
            if (G1 ** x - G2 ** y > 0 and
                G2 ** z - G1 ** w > 0):
                solution = {}
                solution[ord(group1[0]) - 0x41] = G1 ** exp11
                solution[ord(group1[1]) - 0x41] = G1 ** exp12
                solution[ord(group1[2]) - 0x41] = G1
                solution[ord(group2[0]) - 0x41] = G2 ** exp21
                solution[ord(group2[1]) - 0x41] = G2 ** exp22
                solution[ord(group2[2]) - 0x41] = G2
                return solution

    return None

def Check(car, solution):
    for (upper, flg, lower) in car:
        uv = Evaluate(solution, upper)
        lv = Evaluate(solution, lower)
        if flg:
            if not (uv - lv > 0): return False
        else:
            if not (uv - lv >= 0): return False

    return True

def GiveUp():
    sys.exit(1)

def PrintSolution(solution):
    print '6 1'

    for index in xrange(6):
        v = solution[index]
        print v

def main(args):
    data = raw_input()
    car = ParseCar(data)
    ptn, groups = MatchPattern(car)
    if not ptn:
        print >>sys.stderr, 'Failed to match pattern'
        GiveUp()

    solution = Solve(car, groups[0], groups[1])
    if solution is None:
        print >>sys.stderr, 'Failed to solve'
        GiveUp()

    if not (Check(car, solution)):
        print >>sys.stderr, 'Failed to check'
        GiveUp()

    PrintSolution(solution)

if __name__ == '__main__':
    main(sys.argv)


def main():
    pass


if __name__ == '__main__':
    main()
